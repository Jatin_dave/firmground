//
//  UITextField+Extra.h
//  QuizSocialNetwork
//
//  Created by Trainee on 2/19/15.
//  Copyright (c) 2015 ifuturz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Extra)
-(void)setDefaultTextFieldStyle:(NSString *)Placeholder;
-(void)setDefaultTextFieldStyleForUserName:(NSString *)Placeholder;
-(void)setDefaultTextFieldStyleForUserNamewithPadingNO:(NSString *)Placeholder;


-(void)setTextFieldForSelectAge;
-(void)setDefaultTextFieldForAns:(NSString *)str;
-(void)setDefaultTextFieldForAnsInCreate;
@end
