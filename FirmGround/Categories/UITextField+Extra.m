//
//  UITextField+Extra.m
//  QuizSocialNetwork
//
//  Created by Trainee on 2/19/15.
//  Copyright (c) 2015 ifuturz. All rights reserved.
//

#import "UITextField+Extra.h"
#import "Constant.h"
@implementation UITextField (Extra)
-(void)setDefaultTextFieldStyle:(NSString *)Placeholder
{
    self.borderStyle=UITextBorderStyleNone;
    [self.layer setCornerRadius:IS_IPAD_OR_IPHONE(20, 10)];
    [self.layer setMasksToBounds:YES];
    [self setPlaceholder:Placeholder];
    [self setFont:FONT_HELV_LIGHT(IS_IPAD_OR_IPHONE(20, 12))];
    [self setBackgroundColor:[UIColor whiteColor]];
    self.attributedPlaceholder=[[NSAttributedString alloc]initWithString:Placeholder
                                                    attributes:@{NSForegroundColorAttributeName:GET_COLOR_WITH_RGB(69, 70, 76, 1)}];
    
    

}
-(void)setDefaultTextFieldStyleForUserName:(NSString *)Placeholder
{
    self.borderStyle=UITextBorderStyleNone;
    [self.layer setMasksToBounds:YES];
    [self setPlaceholder:Placeholder];
    self.attributedPlaceholder=[[NSAttributedString alloc]initWithString:Placeholder
                                                              attributes:@{NSForegroundColorAttributeName: [self colorWithHexString:@"#d2d2d2"]}];
    [self setFont:FONT_HELV_LIGHT(IS_IPAD_OR_IPHONE(20, 14))];
    [self setTextColor:[self colorWithHexString:@"#d2d2d2"]];
    NSInteger width = 0;
    
    if (IS_IPHONE5) {
        
        width = 40;
        
    }else if (IS_IPHONE6)
    {
        width = 50;
    }else if (IS_IPHONE6p)
    {
        width = 60;
    }
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 20)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
}
-(void)setDefaultTextFieldStyleForUserNamewithPadingNO:(NSString *)Placeholder
{
    self.borderStyle=UITextBorderStyleNone;
    [self.layer setMasksToBounds:YES];
    [self setPlaceholder:Placeholder];
    self.attributedPlaceholder=[[NSAttributedString alloc]initWithString:Placeholder
                                                              attributes:@{NSForegroundColorAttributeName: [self colorWithHexString:@"#d2d2d2"]}];
    [self setFont:FONT_HELV_LIGHT(IS_IPAD_OR_IPHONE(20, 14))];
    [self setTextColor:[self colorWithHexString:@"#d2d2d2"]];
   
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
@end
