//
//  Constant.h
//  QuizSocialNetwork
//
//  Created by Trainee on 2/18/15.
//  Copyright (c) 2015 ifuturz. All rights reserved.
//

#ifndef QuizSocialNetwork_Constant_h
#define QuizSocialNetwork_Constant_h

typedef enum : NSUInteger {
    English,
    Arabic
} languages;

#define APP_NAME            @"FirmGround"

#define localiSestring(string)  [Language get:string alter:string]
#define LocaliseString(string,alternate)  [Language get:string alter:alternate]


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define SetDefaultValue(value,key)             [[NSUserDefaults standardUserDefaults] setObject:value forKey:key], [[NSUserDefaults standardUserDefaults] synchronize]
#define GetDefaultValue(key)                   [[NSUserDefaults standardUserDefaults] valueForKey:key]
#define RemoveDefaultValue(key)                [[NSUserDefaults standardUserDefaults] removeObjectForKey:key], [[NSUserDefaults standardUserDefaults] synchronize]

#define kDefaultDateFormat           @"MMM dd, yyyy HH:mm:ss"
//#define kDefaultShortDateFormat      @"MMM dd, yyyy"
#define kDefaultShortDateFormat      @"dd - MMM - yyyy"

#define Pushviewcontroller(nameofController)   [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:nameofController] animated:YES];

#define kSILoginTBScreen @"LoginTBScreen"
#define kSISignUpTBScreen   @"SignUpTBScreen"
#define kSICreateGroupVCScreen      @"CreateGroupVCScreen"
#define kSIMainCollectionVC         @"MainCollectionVC"
#define kSITabbarMainController       @"TabbarMainController"
#import "SharedManager.h"
//#import "Reachability.h"



#define IMAGE_WITH_NAME_AND_RENDER_MODE(imgName, ImageRenderMode) [[UIImage imageNamed:imgName] imageWithRenderingMode:ImageRenderMode == kImageRenderModeOriginal ? UIImageRenderingModeAlwaysOriginal : (ImageRenderMode == kImageRenderModeTemplate ? UIImageRenderingModeAlwaysTemplate : UIImageRenderingModeAutomatic)]
typedef enum : NSInteger {
    kImageRenderModeOriginal = 0,
    kImageRenderModeTemplate,
    kImageRenderModeAutomatic,
} ImageRenderMode;
//----------------------------------------------------------------------------------------------------------------
#pragma mark - Width-Height
//----------------------------------------------------------------------------------------------------------------

#define IS_IPHONE6p         CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size,CGSizeMake(1242,2208))
#define IS_IPHONE6          CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size,CGSizeMake(750,1334))
#define IS_IPHONE5          CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size,CGSizeMake(640,1136))
#define IS_IPHONE4          CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size,CGSizeMake(640,960))
#define IS_IPHONE           (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?YES:NO
#define IS_IPAD             (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?YES:NO
#define IS_IOS7             SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")
#define SCREEN_WIDTH         [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT        [UIScreen mainScreen].bounds.size.height
#define navSize             self.navigationController.navigationBar.frame.size.height
/****** Displaying Messages when requesting on Server.. ******/


#define ShareOBJ            [SharedManager sharedInstance]

#pragma mark General Web Setting..

// Live Domain....
//#define API_URL     @"http://192.168.1.70/MostLikeApp/api/values/"

//#define API_URL     @"http://ibitabyte.com/guber/api/"
#define API_URL     @"http://103.240.34.162:9002//api/Mobile/"


#define API_URL_With_(Mode)                 [NSString stringWithFormat:@"%@%@",API_URL,Mode]

#define ImgUsers @"http://67.205.163.48/assets/files/providers/"
#define Imgpackages @"http://67.205.163.48/assets/files/packages"
#define Imggames @"http://67.205.163.48/assets/files/providers/"

#define ImgUsersAPP @"http://67.205.163.48/assets/files/users/"
#define GET_ImgForUserAPP(Name) [NSString stringWithFormat:@"%@%@",ImgUsersAPP,Name]

#define GET_ImgForProviders(Name) [NSString stringWithFormat:@"%@%@",ImgUsers,Name]
#define GET_ImgForpackages(Name) [NSString stringWithFormat:@"%@%@",Imgpackages,Name]
#define GET_ImgForgames(Name) [NSString stringWithFormat:@"%@%@",Imggames,Name]
#define GET_ThumbImgForProviders(Name) [NSString stringWithFormat:@"%@thumb/%@",ImgUsers,Name]
#define GET_ThumbImgForpackages(Name) [NSString stringWithFormat:@"%@thumb/%@",Imgpackages,Name]
#define GET_ThumbImgForgames(Name) [NSString stringWithFormat:@"%@thumb/%@",Imggames,Name]


#define kSEARCHTEXT                          @"Search"

#define kRequestTimeOut                      60

#define kAPIstatus                           @"status"
#define kAPIMessage                          @"message"
#define RESPONSE_STATUS_1                    @"1"
#define RESPONSE_STATUS_0                    @"0"
#define kInternetConnectedNotification            @"Please check your internet connection"

#define ImgURL(url)   [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]]

#define LOG_EXCEPTION(exception)           NSLog(@"%s %d => %@",__FUNCTION__,__LINE__,[exception debugDescription])

//----------------------------------------------------------------------------------------------------------------
#pragma mark - Colors
//----------------------------------------------------------------------------------------------------------------
#define kStartColor @"#26a9e0"
#define kEndColor @"#35b87b"


#define   GRADIENT_IMG          [GradientImage getGradientImageWithStartColor:[Colors colorFromHexString:kStartColor] endColor:[Colors colorFromHexString:kEndColor]]
#define CLEAR_COLOR                  [UIColor clearColor]
#define BLUE_COLOR                   [UIColor blueColor]
#define RED_COLOR                    [UIColor redColor]
#define WHITE_COLOR                  [UIColor whiteColor]
#define GREEN_COLOR                  [UIColor greenColor]
#define BLACK_COLOR                  [UIColor blackColor]
#define LIGHTGRAY_COLOR              [UIColor lightGrayColor]
#define UI_DEFAULT_COLOR             [UIColor colorWithRed:0/255.0f green:146/255.0f blue:212/255.0f alpha:1]
#define GET_COLOR_WITH_RGB(r,g,b,a)  [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define YELLOW_LIGHT_RGB             [UIColor colorWithRed:243/255.0f green:222/255.0f blue:167/255.0f alpha:1]
#define APPGREENCOLOR               [UIColor colorWithRed:53/255.0f green:125/255.0f blue:31/255.0f alpha:1]
#define APPGRAYCOLOR               [UIColor colorWithRed:114/255.0f green:86/255.0f blue:50/255.0f alpha:1]

#define BG_COLOR                     [UIColor colorWithRed:243/255.0f green:243/255.0f blue:243/255.0f alpha:1]
#define APP_DELEGATE                (AppDelegate *)[[UIApplication sharedApplication] delegate]

//----------------------------------------------------------------------------------------------------------------
#pragma mark - Fonts
//----------------------------------------------------------------------------------------------------------------

#define FONT_HELV_LIGHT(_size)   [UIFont fontWithName:@"Helvetica-Light" size:_size]
#define FONT_HELV_BOLD(_size)   [UIFont fontWithName:@"Helvetica-Bold" size:_size]
#define FONT_HELV_Black(_size)   [UIFont fontWithName:@"Helvetica-Regular" size:_size]
#define FONT_HELV_Light(_size)   [UIFont fontWithName:@"Gotham-Light" size:_size]
#define FONT_HELV_Medium(_size)   [UIFont fontWithName:@"GothamMedium" size:_size]


#define ScreenTitleFontWithOutBold KSetFont(kDefaultFontName, (IS_IPAD)?18:16.0f)
#define GET_IMAGE_WITH_NAME(name) [UIImage imageNamed:(IS_IPAD)?[NSString stringWithFormat:@"%@~iPad",name]:name]
#define IS_IPAD_OR_IPHONE(Ipad,Iphone)          ((IS_IPAD)?Ipad:Iphone)            
#define NAVHIEGHT 44.0

/****** Displaying Messages when requesting on Server.. ******/
#pragma mark Progress Messages..
#define kProgressAuthenticate                   @"Authenticating User..."
#define kProgressSignUp                         @"Registering User..."
#define kProgressForgotPass                     @"Sending Email..."
#define kProgress                               @"Processing..."

#define kProgressFailed                         @"Failed to Fetch data from the server!"
/****** Displaying Messages when requesting on Server.. ******/
#pragma mark Alert Messages..
#define kMessageBlankField                      @"Please Fill All The Details"
#define kMessageCountryValidation               @"Please set your country."
#define kMessageAgeValidation                   @"Please set your age."
#define kMessageProfileImageValidation          @"Please set profile photo.."
#define kMessageGenderValidation                @"Please set your gender"
#define kMessageBriefValidation                 @"Please fill out your brief intro"
#define kMessageUserNameValidation              @"Please enter your User Name.!"
#define kMessageCountryIntrestValidation        @"Please set country of your interest. OR location range for search "
#define kMessageLocationRangeValidation         @"Please set the location range for search."
#define kMessageAgeRangeValidation              @"Please set the age range of your interest."
#define kMessageGenderOfInterestValidation      @"Please set the gender of your interest."
#define kMessageEmailValidValidation            @"Please enter valid email."
#define kMessageEmailEmptyValidation            @"Please enter your email."
#define kMessageFeedBackValidation              @"Please enter Feedback text."
#define kMessageTitleValidation                 @"Please enter Title."
#define kMessageFeedBackThanksValidatoin       @"Thanks for your feedback."
#define kMessagePasswordValidValidation            @"Please enter valid password."
#define kMessageConfPasswordValidValidation            @"Your password and confirmation password do not match."

#define kMessageForPlatFomrValidation          @"Please select one or more gaming platform(s)."

#define kMessageNoUsersfound                     @"Sorry,No user Found!!!"

#define kMessageForPayMentSelection                     @"Please select one payment method."


#define Mode_Login        @"Login"
#define Mode_RegisterLogin        @"RegisterLogin"
#define Mode_GetProfile         @"GetProfile"
#define Mode_UpdateProfile          @"UpdateProfile"
#define Mode_UpdateImage            @"UpdateImage"
#define Mode_Logout            @"Logout"
#define Mode_AddChatkey            @"AddChatkey"
#define Mode_AddGroupandContact            @"AddGroupandContact"
#define Mode_AddContactinGroup            @"AddContactinGroup"
#define Mode_DeleteContactinGroup            @"DeleteContactinGroup"
#define Mode_GetGame            @"GetGame"
#define Mode_LastGame            @"LastGame"
#define Mode_CreateGame            @"CreateGame"
#define Mode_UpdateGame            @"UpdateGame"
#define Mode_GetGameContactInOut            @"GetGameContactInOut"
#define Mode_SetGameContactInOut           @"SetGameContactInOut"
#define Mode_CancelGame           @"CancelGame"
#define Mode_ForgotPassword         @"ForgotPassword"


#define kuserid                             @"userid"
#define kusername                        @"username"
#define kpassword                        @"password"
#define kandroidId                        @"androidId"
#define kdevicetoken                      @"devicetoken"
#define kisiosandroid                        @"isiosandroid"
#define kNotAuthorize                        @"NotAuthorize"
#define kErrorStatus                        @"ErrorStatus"
#define kErrorMessage                        @"ErrorMessage"
#define kData                           @"Data"
#define kerrorcode                        @"errorcode"
#define kerrormessage                        @"errormessage"
#define ktoken                        @"token"

#define kcreateddate                @"createddate"
#define kdob                        @"dob"
#define kemergencymobile            @"emergencymobile"
#define kemergencyname              @"emergencyname"
#define klocation                   @"location"
#define kusername                   @"username"
#define kename                      @"ename"
#define kemobile                    @"emobile"
#define kimageurl                   @"imageurl"

#define kphoneNumber                @"phoneNumber"
#define kgivenName                  @"givenName"
#define kfamilyName                 @"familyName"
#define kemailAddresses             @"emailAddresses"

#define kfirst_name                     @"first_name"
#define kname                        @"name"
#define kmobile                        @"mobile"
#define kisSelected                     @"kisSelected"

#define KOnlineAlertMessage(hours)  [NSString stringWithFormat:@"Provider is On-Line for %@ hours since last time app open.Unless provider expressely goes off Line",hours]hours

#define kNotificationForCheckReuest         @"NotificationForCheckReuest"

#endif
