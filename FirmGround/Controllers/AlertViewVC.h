//
//  AlertViewVC.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/22/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertViewDelegate <NSObject>

-(void)alertViewOKactionWithString:(NSString*)strTextField;

-(void)alerViewCancelAction;

@end

@interface AlertViewVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)btnCancel:(id)sender;
- (IBAction)btnOk:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (strong, nonatomic) IBOutlet UILabel *lblAlertTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfAlertTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imgOfTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tesxtfieldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfImageLeading;
@property (weak, nonatomic) IBOutlet UIImageView *headerBGimage;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak,nonatomic) id<AlertViewDelegate>alertViewDelegate;

@end
