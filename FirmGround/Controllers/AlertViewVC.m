//
//  AlertViewVC.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/22/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AlertViewVC.h"
#import "LoginTBScreen.h"
#import "Colors.h"
#import "GradientImage.h"
#import "Constant.h"
#import "UITextField+Extra.h"
#import "Validation.h"

@interface AlertViewVC ()

@end

@implementation AlertViewVC
@synthesize lblAlertTitle = _lblAlertTitle;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_tfAlertTextField setDefaultTextFieldStyleForUserName:@"Enter email"];

    
    [_headerBGimage setImage:[GradientImage getGradientImageWithStartColor:[Colors colorFromHexString:kStartColor] endColor:[Colors colorFromHexString:kEndColor]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
-(void)viewDidLayoutSubviews{
    
    NSLog(@"%f",self.view.frame.size.height);
    if (self.view.frame.size.height==736) {
        
        _containerViewHeight.constant=230;
        _tesxtfieldHeight.constant=45;
        _tfImageLeading.constant=27;
        
    }
    else if (self.view.frame.size.height==667){
        _containerViewHeight.constant=200;
        _tesxtfieldHeight.constant=40;
    }
    else if (self.view.frame.size.height==568){
        _containerViewHeight.constant=180;
        _tesxtfieldHeight.constant=35;
    }
    
}

//-(void)hide{
//
//    LoginTBScreen *login=(LoginTBScreen*)[self parentViewController];
//    [login hideAlertView];
//
//}

- (IBAction)btnCancel:(id)sender {
    [[self alertViewDelegate]alerViewCancelAction];
}

- (IBAction)btnOk:(id)sender {
    

        [[self alertViewDelegate]alertViewOKactionWithString:_tfAlertTextField.text];

}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch=[[event allTouches]anyObject];
    CGPoint location=[touch locationInView:self.view];
    
    if (CGRectContainsPoint(_containerView.frame, location)) {
        
        
    }else{
        // [self hide];
        [[self alertViewDelegate]alerViewCancelAction];
    }
    
}
#pragma mark
#pragma mark --- CustomAlert
-(void)AlertWithString:(NSString*)strError withType :(NSInteger)alertType
{
    
    CWStatusBarNotification *notification = [CWStatusBarNotification new];
    if (alertType==0) {
        notification.notificationLabelBackgroundColor = [UIColor colorWithRed:195/255.0 green:66/255.0 blue:45/255.0 alpha:1];
    }
    else if(alertType==1)
    {
        notification.notificationLabelBackgroundColor = APPGREENCOLOR;
    }
    notification.notificationLabelTextColor = [UIColor whiteColor];
    //    notification.notificationLabelFont =FONT_HELV_LIGHT(12);
    notification.notificationAnimationInStyle =CWNotificationAnimationStyleTop;
    notification.notificationAnimationOutStyle =CWNotificationAnimationStyleTop;
    [notification displayNotificationWithMessage:strError
                                     forDuration:2.0f];
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
@end
