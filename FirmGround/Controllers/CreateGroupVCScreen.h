//
//  CreateGroupVCScreen.h
//  FirmGround
//
//  Created by Admin on 24/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ContactsUI/ContactsUI.h>
#import "ContactList.h"
#import "CWStatusBarNotification.h"
#import "GradientImage.h"
#import "Colors.h"
#import "AlertViewVC.h"
#import "Validation.h"
#import "SVProgressHUD.h"
#import "AFNetworkManager.h"
@interface CreateGroupVCScreen : UIViewController<UITableViewDelegate,UITableViewDataSource,CNContactViewControllerDelegate, CNContactPickerDelegate,UISearchBarDelegate,AlertViewDelegate>
{
    NSMutableArray *arrcontactList,*arrSelectedContact,*arrSearchCont;
    BOOL isSearch;
    UIView *blurView;

}
@property (strong, nonatomic) AlertViewVC *alertview;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UISearchBar *objSearch;
@property (strong, nonatomic) IBOutlet UIView *CollectionMainView;
@property (strong, nonatomic) IBOutlet UITableView *tblList;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewToplist;
@property (strong, nonatomic) IBOutlet UILabel *lblCountBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *TopViewHConsant;
@end
