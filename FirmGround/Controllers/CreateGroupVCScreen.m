//
//  CreateGroupVCScreen.m
//  FirmGround
//
//  Created by Admin on 24/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CreateGroupVCScreen.h"
#import "CreateGroupCell.h"
#import "CreateGroupCVCell.h"
#import "User.h"
#import "UIBarButtonItem+Extra.h"
@interface CreateGroupVCScreen ()

@end

@implementation CreateGroupVCScreen
@synthesize alertview = _alertview;
#pragma mark
#pragma mark - LifeCycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *imgBackground = [[UIImage imageNamed:@"background"]stretchableImageWithLeftCapWidth:0.0
                                                                                    topCapHeight:5.0];
    UIImageView *imgviewBG =[[UIImageView alloc]initWithImage:imgBackground];
    _tblList.backgroundView = imgviewBG;
    
    
    self.title = @"New Group";
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    UIImage *currentImgae =  [[UIImage imageNamed:@"gradinat"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [self.navigationController.navigationBar setBackgroundImage:currentImgae forBarMetrics:UIBarMetricsDefault];
    
    UIImage *faceImage = [UIImage imageNamed:@"iconback"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    [face addTarget:self action:@selector(PopToView:) forControlEvents:UIControlEventTouchUpInside];
    face.bounds = CGRectMake( 0, 0, faceImage.size.width, faceImage.size.height );
    [face setImage:faceImage forState:UIControlStateNormal];
    UIBarButtonItem *LEftButton = [[UIBarButtonItem alloc] initWithCustomView:face];
    [self.navigationItem setLeftBarButtonItem:LEftButton];
    
    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Create Group" style:UIBarButtonItemStylePlain target:self action:@selector(btnToCreateGroup:)];
    self.navigationItem.rightBarButtonItem = backButtonItem;
    [backButtonItem barButtonDesingn];

    
    arrcontactList = [[NSMutableArray alloc]init];
    arrSearchCont = [[NSMutableArray alloc]init];
    _TopViewHConsant.constant = 0;
    
//    for (int i=0; i<21; i++) {
//        
//        NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
//        [dic setObject:[NSString stringWithFormat:@"name_%d",i] forKey:kfirst_name];
//        
//        User *obj =[[User alloc]initWithObject:dic];
//        obj.isSelected = 0;
//        [arrcontactList addObject:obj];
//    }
    
    arrSelectedContact = [[NSMutableArray alloc]initWithCapacity:arrcontactList.count];
    _lblCountBottom.text = [NSString stringWithFormat:@"%lu of %lu Selected",(unsigned long)arrSelectedContact.count,(unsigned long)arrcontactList.count];
    
    [self loadContactList];
    
    _bottomView.backgroundColor =[UIColor colorWithPatternImage:GRADIENT_IMG];
    
    blurView=[[UIView alloc]initWithFrame:self.view.frame];
    blurView.backgroundColor=[UIColor darkGrayColor];
    blurView.alpha=0;
    [self.navigationController.view addSubview:blurView];
    [self addAlertView];


}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationController.navigationBar.hidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark
#pragma mark - Tableview deleagte and datasource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (isSearch) {
        return arrSearchCont.count;
    }
    return arrcontactList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    return 82;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CreateGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreateGroupCell" forIndexPath:indexPath];
    UIColor  *bordercolor = [cell InitwithCustumCellwithIndexpath:indexPath];
    
    ContactList *objUser;
    if (isSearch) {
        objUser =[arrSearchCont objectAtIndex:indexPath.row];
        
    }else{
        objUser =[arrcontactList objectAtIndex:indexPath.row];
    }
    
    objUser.borderClor = bordercolor;
    
    cell.lblName.text = [NSString stringWithFormat:@"%@",objUser.givenName];
    cell.btnSelected.selected = (objUser.isSelected ==0) ?NO:YES;
    
    return cell;
 }
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (arrSelectedContact.count <= 100) {
        
        ContactList *objUser;

        if (isSearch) {
            
            objUser =[arrSearchCont objectAtIndex:indexPath.row];

        }else{
            
            objUser =[arrcontactList objectAtIndex:indexPath.row];

        }
        
        CreateGroupCell *cell =[tableView cellForRowAtIndexPath:indexPath];
        if (objUser.isSelected==0) {
            
            cell.btnSelected.selected = YES;
            objUser.isSelected = 1;
            
            [arrSelectedContact addObject:objUser];
            
        }else{
            cell.btnSelected.selected = NO;
            objUser.isSelected = 0;
            [arrSelectedContact removeObject:objUser];
            
            
        }
        
        if (arrSelectedContact.count >0) {
            
            _TopViewHConsant.constant = 66;
            
        }else{
            _TopViewHConsant.constant = 0;
            
        }
        
        _lblCountBottom.text = [NSString stringWithFormat:@"%lu of %lu Selected",(unsigned long)arrSelectedContact.count,(unsigned long)arrcontactList.count];
        [arrcontactList replaceObjectAtIndex:indexPath.row withObject:objUser];
        
        [_collectionViewToplist reloadData];

    }else{
        
        [self AlertWithString:@"100+" withType:1];
    }
    
    
}
#pragma mark
#pragma mark - CollectionView deleagte and datasource methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrSelectedContact.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
   CreateGroupCVCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CreateGroupCVCell" forIndexPath:indexPath];
    
    ContactList *obj =[arrSelectedContact objectAtIndex:indexPath.row];
    
    [cell InitwithCustumCellwithIndexpath:indexPath withUser:obj];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    ContactList *obj =[arrSelectedContact objectAtIndex:indexPath.row];
    
    if (obj.isSelected==0) {
        
        obj.isSelected = 1;
        
    }else{
        
        obj.isSelected = 0;
    }
    
    [arrSelectedContact removeObject:obj];
    
    if (arrSelectedContact.count >0) {
        
        _TopViewHConsant.constant = 66;
        
    }else{
        _TopViewHConsant.constant = 0;
        
    }
    
    _lblCountBottom.text = [NSString stringWithFormat:@"%lu of %lu Selected",(unsigned long)arrSelectedContact.count,(unsigned long)arrcontactList.count];

    [_tblList reloadData];
    [_collectionViewToplist reloadData];

}
#pragma mark
#pragma mark - UIbuttons Actions
-(IBAction)PopToView:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnToCreateGroup:(id)sender{
    
    [self showAlertView];
    
    
}
- (IBAction)btnSelectedUnselectedUsers:(UIButton *)sender {
    
    
}
#pragma mark
#pragma mark --- Contact List
-(void)reloadContactList {
    [arrcontactList removeAllObjects];
    [self loadContactList];
}
-(void)loadContactList {
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        
        //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
        
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
        
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
             // Contact one each function block is executed whenever you get
             NSString *phoneNumber = @"";
             if( contact.phoneNumbers)
                 phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
             
             NSLog(@"phoneNumber = %@", phoneNumber);
             NSLog(@"givenName = %@", contact.givenName);
             NSLog(@"familyName = %@", contact.familyName);
             NSLog(@"email = %@", contact.emailAddresses);
             
             NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
             
             [dic setObject:phoneNumber forKey:kphoneNumber];
             [dic setObject:contact.givenName forKey:kgivenName];
             [dic setObject:contact.familyName forKey:kfamilyName];
             [dic setObject:contact.emailAddresses forKey:kemailAddresses];

             ContactList *objConta = [[ContactList alloc]initWithObject:dic];
             
             [arrcontactList addObject:objConta];
         }];
      
        [_tblList reloadData];
    }
    
}

#pragma mark -
#pragma mark CNContactPickerDelegate
/*!
 * @abstract Invoked when the picker is closed.
 * @discussion The picker will be dismissed automatically after a contact or property is picked.
 */
- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
    NSLog(@"User canceled picker");
}
#pragma mark
#pragma mark --- SearchBar Delegates

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    return YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
//    searchBar.autocapitalizationType = UITextAutocapitalizationTypeSentences;
//    [_objSearch setShowsCancelButton:YES animated:YES];
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    
        isSearch = NO;
    
        [searchBar setText:@""];
        [_objSearch setShowsCancelButton:NO animated:NO];
        
        [searchBar resignFirstResponder];
        [_tblList reloadData];
    
//        if ([arruserList count] == 0 ) {
//            [lblNoUser setHidden:NO];
//        }
//        else
//        {
//            [lblNoUser setHidden:YES];
//        }
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""]) {
        
        isSearch =NO;
    }else{
        
        isSearch = YES;
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.givenName contains[c] %@",
                                  searchText];
        arrSearchCont =(NSMutableArray*)[arrcontactList filteredArrayUsingPredicate:predicate];

    }
    
    [_tblList reloadData];
}

#pragma mark
#pragma mark --- CustomAlert
-(void)AlertWithString:(NSString*)strError withType :(NSInteger)alertType
{
    
    CWStatusBarNotification *notification = [CWStatusBarNotification new];
    if (alertType==0) {
        notification.notificationLabelBackgroundColor = [UIColor colorWithRed:195/255.0 green:66/255.0 blue:45/255.0 alpha:1];
    }
    else if(alertType==1)
    {
        notification.notificationLabelBackgroundColor = APPGREENCOLOR;
    }
    notification.notificationLabelTextColor = [UIColor whiteColor];
    //    notification.notificationLabelFont =FONT_HELV_LIGHT(12);
    notification.notificationAnimationInStyle =CWNotificationAnimationStyleTop;
    notification.notificationAnimationOutStyle =CWNotificationAnimationStyleTop;
    [notification displayNotificationWithMessage:strError
                                     forDuration:2.0f];
    
}
#pragma mark AlertViewManage

-(void)addAlertView{
    
    _alertview=[self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewVC"];
    _alertview.alertViewDelegate = self;
    
    [self.navigationController addChildViewController:_alertview];
    _alertview.view.frame = self.view.frame;
    [self.navigationController.view addSubview:_alertview.view];
    [_alertview didMoveToParentViewController:self];
    _alertview.lblAlertTitle.text = @"Group Name";
    _alertview.tfAlertTextField.placeholder = @"Group Name";

    _alertview.view.alpha=0;

}
-(void)showAlertView{
    [UIView animateWithDuration:0.3 animations:^{
        [_alertview.tfAlertTextField becomeFirstResponder];
        _alertview.view.alpha=1;
        blurView.alpha=0.5;
        
    }];
}
-(void)hideAlertView{
    [UIView animateWithDuration:0.3 animations:^{
        [_alertview.tfAlertTextField resignFirstResponder];
        [_alertview removeFromParentViewController];
        _alertview.view.alpha=0;
        blurView.alpha=0;
        // [blurView removeFromSuperview];
    }];
}
#pragma  mark alertview delegates

-(void)alertViewOKactionWithString:(NSString*)strTextField{
    
    NSLog(@"alertViewOKactionWithString%@",strTextField);
    if (![Validation textFieldLength:strTextField]) {
        
        [self AlertWithString:@"Please Enter GroupName" withType:0];
        
    }else{
        
        [SVProgressHUD showWithStatus:@"Please wait..." ];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        
        NSMutableDictionary *dicSignUp =[[NSMutableDictionary alloc]init];
        
        [dicSignUp setObject:strTextField forKey:kusername];
        
        [AFNetworkManager postOperationServiceCallWithParameter:dicSignUp strURL:API_URL_With_(Mode_ForgotPassword) :^(id sucess)
         {
             
             if ([[sucess valueForKey:kErrorStatus]integerValue]==0) {
                 

                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         [self hideAlertView];
                         Pushviewcontroller(@"MainCollectionVC");

                     });
                     
                 });
             }
             else
             {
                 [self AlertWithString:[sucess valueForKey:@"ErrorMessage"] withType:1];
                 [self hideAlertView];

                 Pushviewcontroller(@"MainCollectionVC");

                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                     });
                     
                 });
                 
             }
             
         } failure:^(NSError *Error) {
             NSLog(@"Fail at %@ ",Error);
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 // time-consuming task
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                 });
             });
             
             
         }];

    }
    
}

-(void)alerViewCancelAction{
    
    [self hideAlertView];
    
}

@end
