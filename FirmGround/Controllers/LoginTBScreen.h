//
//  ViewController.h
//  FirmGround
//
//  Created by Admin on 18/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CWStatusBarNotification.h"
#import "AFNetworkManager.h"
#import "SVProgressHUD.h"
#import "AlertViewVC.h"
@import FirebaseAuth;

@interface LoginTBScreen : UITableViewController<AlertViewDelegate>
{
    
    IBOutlet UIButton *btnLogin;
    UIView *blurView;
    AlertViewVC *alertview;
}
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
-(void)hideAlertView;
@end

