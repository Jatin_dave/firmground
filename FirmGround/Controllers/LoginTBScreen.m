//
//  ViewController.m
//  FirmGround
//
//  Created by Admin on 18/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "LoginTBScreen.h"
#import "Constant.h"
#import "UITextField+Extra.h"
#import "Validation.h"
#import "TabbarMainController.h"
#import "MainCollectionVC.h"

@interface LoginTBScreen ()

@end

@implementation LoginTBScreen

#pragma mark
#pragma mark - LifeCycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *imgBackground = [[UIImage imageNamed:@"background"]stretchableImageWithLeftCapWidth:0.0
                                                                                    topCapHeight:5.0];
    UIImageView *imgviewBG =[[UIImageView alloc]initWithImage:imgBackground];
    self.tableView.backgroundView = imgviewBG;
    
    [_txtEmail setDefaultTextFieldStyleForUserName:@"Enter email"];
    [_txtPassword setDefaultTextFieldStyleForUserName:@"Enter password"];
    
    _txtEmail.text = @"Harshil.shah2@ipathsolutions.com";
    _txtPassword.text = @"harshil";
    
    _txtEmail.text = @"mail2davejatin@gmail.com";
    _txtPassword.text = @"123456";


    blurView=[[UIView alloc]initWithFrame:self.view.frame];
    blurView.backgroundColor=[UIColor darkGrayColor];
    blurView.alpha=0;
    [self.view addSubview:blurView];
    
    [self addAlertView];

 //NSLog (@"Font families: %@", [UIFont familyNames]);

 //   NSLog (@"Gotham New family fonts: %@", [UIFont fontNamesForFamilyName:@"Gotham"]);

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden =YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden =NO;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark AlertViewManage

-(void)addAlertView{
    
    alertview=[self.storyboard instantiateViewControllerWithIdentifier:@"AlertViewVC"];
    alertview.alertViewDelegate = self;
    
    [self addChildViewController:alertview];
    alertview.view.frame = self.view.frame;
    [self.view addSubview:alertview.view];
    [alertview didMoveToParentViewController:self];
    
    alertview.view.alpha=0;
}
-(void)showAlertView{
    [UIView animateWithDuration:0.3 animations:^{
        [alertview.tfAlertTextField becomeFirstResponder];
        alertview.view.alpha=1;
        blurView.alpha=0.5;
        
    }];
}
-(void)hideAlertView{
    [UIView animateWithDuration:0.3 animations:^{
        [alertview.tfAlertTextField resignFirstResponder];
        alertview.view.alpha=0;
        blurView.alpha=0;
       // [blurView removeFromSuperview];
    }];
}
#pragma mark
#pragma mark - Tableview deleagte and datasource methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        
        return (SCREEN_HEIGHT/2) -28;
    }
    else if (indexPath.row==3)
    {
        return 93;
    }
    return 50;
}
#pragma mark
#pragma mark - UIbuttons Actions

-(IBAction)RegistrationNavTapped:(id)sender{
     
    Pushviewcontroller(kSISignUpTBScreen);
    
//    MainCollectionVC *mvc=[self.storyboard instantiateViewControllerWithIdentifier:@"MainCollectionVC"];
//    [self.navigationController pushViewController:mvc animated:YES];
//    
}
- (IBAction)btnLoginTapped:(UIButton *)sender {
    
    
    if (![Validation textFieldLength:_txtEmail.text]) {
        
        [self AlertWithString:@"Please Enter Email" withType:0];
        
    }
    
    else if (![Validation validateEmail:_txtEmail.text])
    {
        [self AlertWithString:@"Please Enter Valid Email" withType:0];
        
    }
    else if (![Validation textFieldLength:_txtPassword.text])
    {
        [self AlertWithString:@"Please Enter Password" withType:0];
        
    }
    else{
        
//        [self performSelector:@selector(PushToHome) withObject:nil afterDelay:0.3];

        
        [SVProgressHUD showWithStatus:@"Please wait..." ];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        
        NSMutableDictionary *dicSignUp =[[NSMutableDictionary alloc]init];
        
        [dicSignUp setObject:_txtEmail.text forKey:kusername];
        [dicSignUp setObject:_txtPassword.text forKey:kpassword];
        [dicSignUp setObject:@"and" forKey:kandroidId];
        [dicSignUp setObject:@(123) forKey:kdevicetoken];
        [dicSignUp setObject:@"ios" forKey:kisiosandroid];
        
        
        [AFNetworkManager postOperationServiceCallWithParameter:dicSignUp strURL:API_URL_With_(Mode_Login) :^(id sucess)
         {
             
             if ([[sucess valueForKey:kErrorStatus]integerValue]==0) {
                 
                 NSDictionary *userDetail = [sucess valueForKey:kData];
                 NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:userDetail];
                 [[NSUserDefaults standardUserDefaults] setObject:personEncodedObject forKey:kData];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 
                 SetDefaultValue([[sucess valueForKey:kData] valueForKey:kuserid], kuserid);
                 ShareOBJ.userid = [GetDefaultValue(kuserid) integerValue];
                 
                 SetDefaultValue([[sucess valueForKey:kData] valueForKey:ktoken], ktoken);
                 ShareOBJ.token = GetDefaultValue(ktoken);

                 [self firebaseAuthUsingEmail];
                 
                 
             }
             else
             {
                 [self AlertWithString:[sucess valueForKey:@"ErrorMessage"] withType:1];
                 
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                     });
                     
                 });
                 
             }
             
         } failure:^(NSError *Error) {
             NSLog(@"Fail at %@ ",Error);
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 // time-consuming task
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                 });
             });
             
             
         }];
    }
    
    
}
#pragma mark
#pragma mark - Firebase Auth
-(void)firebaseAuthUsingEmail
{
    [[FIRAuth auth] signInWithEmail:_txtEmail.text password:_txtPassword.text completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        [SVProgressHUD dismiss];

        [self performSelector:@selector(PushToHome) withObject:nil afterDelay:0.2];
        
    }];
}
//- (IBAction)btnNew:(id)sender {
//    
//    MainCollectionVC *mvc=[self.storyboard instantiateViewControllerWithIdentifier:@"MainCollectionVC"];
//    [self.navigationController pushViewController:mvc animated:YES];
//    
//}
- (IBAction)btnTappedToForgotPassword:(id)sender {
    
    [self showAlertView];
    
}
#pragma mark
#pragma mark -- TextFeild delegates and datasource

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
#pragma mark
#pragma mark --- cutom methods
-(void)PushToHome
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIStoryboard *tabStory  =[UIStoryboard storyboardWithName:@"TabStoryBord" bundle:nil];
        TabbarMainController *objTab = [tabStory instantiateViewControllerWithIdentifier:@"TabbarMainController"];
        
        
        [self.navigationController pushViewController:objTab animated:YES];
    });
   
    
}
#pragma mark
#pragma mark --- CustomAlert
-(void)AlertWithString:(NSString*)strError withType :(NSInteger)alertType
{
    
    CWStatusBarNotification *notification = [CWStatusBarNotification new];
    if (alertType==0) {
        notification.notificationLabelBackgroundColor = [UIColor colorWithRed:195/255.0 green:66/255.0 blue:45/255.0 alpha:1];
    }
    else if(alertType==1)
    {
        notification.notificationLabelBackgroundColor = APPGREENCOLOR;
    }
    notification.notificationLabelTextColor = [UIColor whiteColor];
//    notification.notificationLabelFont =FONT_HELV_LIGHT(12);
    notification.notificationAnimationInStyle =CWNotificationAnimationStyleTop;
    notification.notificationAnimationOutStyle =CWNotificationAnimationStyleTop;
    [notification displayNotificationWithMessage:strError
                                     forDuration:2.0f];
    
}

#pragma  mark alertview delegates

-(void)alertViewOKactionWithString:(NSString*)strTextField{
    
    NSLog(@"alertViewOKactionWithString%@",strTextField);
    
    if (![Validation textFieldLength:strTextField]) {
        
        [self AlertWithString:@"Please Enter Email" withType:0];
        
    }

    else if (![Validation validateEmail:strTextField])
    {
        
        [self AlertWithString:@"Please Enter Valid Email" withType:1];
        
        
    }
    else{
        
        [SVProgressHUD showWithStatus:@"Please wait..." ];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        
        NSMutableDictionary *dicSignUp =[[NSMutableDictionary alloc]init];
        
        [dicSignUp setObject:strTextField forKey:kusername];
        
        [AFNetworkManager postOperationServiceCallWithParameter:dicSignUp strURL:API_URL_With_(Mode_ForgotPassword) :^(id sucess)
         {
             
             if ([[sucess valueForKey:kErrorStatus]integerValue]==0) {
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         [self hideAlertView];

                     });
                     
                 });
             }
             else
             {
                 [self AlertWithString:[sucess valueForKey:@"ErrorMessage"] withType:1];
                 
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                     });
                     
                 });
                 
             }
             
         } failure:^(NSError *Error) {
             NSLog(@"Fail at %@ ",Error);
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 // time-consuming task
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                 });
             });
             
             
         }];
    }

    
}

-(void)alerViewCancelAction{
    
    [self hideAlertView];
    
}

@end
