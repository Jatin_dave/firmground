//
//  SignUpTBScreen.h
//  FirmGround
//
//  Created by Admin on 22/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CWStatusBarNotification.h"
#import "AFNetworkManager.h"
#import "SVProgressHUD.h"

@interface SignUpTBScreen : UITableViewController
{
    
}
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UITextField *txtConfrimPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtContactNum;
@end
