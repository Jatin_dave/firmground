//
//  SignUpTBScreen.m
//  FirmGround
//
//  Created by Admin on 22/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "SignUpTBScreen.h"
#import "Constant.h"
#import "UITextField+Extra.h"
#import "Validation.h"
#import "TabbarMainController.h"
#import "MainCollectionVC.h"

@interface SignUpTBScreen ()
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;

@end

@implementation SignUpTBScreen
#pragma mark
#pragma mark - LifeCycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *imgBackground = [[UIImage imageNamed:@"background"]stretchableImageWithLeftCapWidth:0.0
                                                                                    topCapHeight:5.0];
    UIImageView *imgviewBG =[[UIImageView alloc]initWithImage:imgBackground];
    self.tableView.backgroundView = imgviewBG;
    
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    UIImage *currentImgae =  [[UIImage imageNamed:@"gradinat"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [self.navigationController.navigationBar setBackgroundImage:currentImgae forBarMetrics:UIBarMetricsDefault];
    
    UIImage *faceImage = [UIImage imageNamed:@"iconback"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    [face addTarget:self action:@selector(PopToHome:) forControlEvents:UIControlEventTouchUpInside];
    face.bounds = CGRectMake( 0, 0, faceImage.size.width, faceImage.size.height );
    [face setImage:faceImage forState:UIControlStateNormal];
    UIBarButtonItem *LEftButton = [[UIBarButtonItem alloc] initWithCustomView:face];
    [self.navigationItem setLeftBarButtonItem:LEftButton];
    
//    UIImage *faceImage1 = [UIImage imageNamed:@"correct"];
//    UIButton *btnSignUp = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnSignUp addTarget:self action:@selector(btnSignUpTapped:) forControlEvents:UIControlEventTouchUpInside];
//    btnSignUp.bounds = CGRectMake( 0, 0, faceImage1.size.width, faceImage1.size.height );
//    [btnSignUp setImage:faceImage1 forState:UIControlStateNormal];
//    UIBarButtonItem *RightButton = [[UIBarButtonItem alloc] initWithCustomView:btnSignUp];
//    [self.navigationItem setRightBarButtonItem:RightButton];
    
    [_txtUsername setDefaultTextFieldStyleForUserName:@"Enter username"];
    [_txtEmail setDefaultTextFieldStyleForUserName:@"Enter email"];
    [_txtPassword setDefaultTextFieldStyleForUserName:@"Enter password"];
    [_txtConfrimPassword setDefaultTextFieldStyleForUserName:@"Confrim password"];
    [_txtContactNum setDefaultTextFieldStyleForUserName:@"Enter number"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.translucent = NO;

}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    _btnSignUp.layer.cornerRadius=_btnSignUp.frame.size.height/2;
}


#pragma mark
#pragma mark - UIbuttons Actions
- (IBAction)btnSignUpTapped:(UIButton *)sender {
    
    
    if (![Validation textFieldLength:_txtUsername.text]) {
        
        [self AlertWithString:@"Please Enter Username" withType:0];
        
    }
    else if (![Validation textFieldLength:_txtEmail.text]) {
        
        [self AlertWithString:@"Please Enter Email" withType:0];
        
    }
    else if (![Validation validateEmail:_txtEmail.text])
    {
        [self AlertWithString:@"Please Enter Valid Email" withType:0];
        
    }
    else if (![Validation textFieldLength:_txtPassword.text])
    {
        [self AlertWithString:@"Please Enter Password" withType:0];
        
    }
    else if ([_txtPassword.text length]<6)
    {
        [self AlertWithString:@"Password Minimum contains atleast 6 characters" withType:0];
        
    }
    else if (![Validation textFieldLength:_txtConfrimPassword.text])
    {
        [self AlertWithString:@"Please Enter Confirm Password" withType:0];
        
    }
    else if (![_txtConfrimPassword.text isEqualToString:_txtPassword.text])
    {
        [self AlertWithString:kMessageConfPasswordValidValidation withType:0];
        
    }
    else if (![Validation textFieldLength:_txtContactNum.text])
    {
        [self AlertWithString:@"Please Enter Contack No" withType:0];
        
    }
    else{
        
        [SVProgressHUD showWithStatus:@"Please wait..." ];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        
        NSMutableDictionary *dicSignUp =[[NSMutableDictionary alloc]init];
        
        [dicSignUp setObject:_txtUsername.text forKey:kname];
        [dicSignUp setObject:_txtEmail.text forKey:kusername];
        [dicSignUp setObject:_txtPassword.text forKey:kpassword];
        [dicSignUp setObject:@"and" forKey:kandroidId];
        [dicSignUp setObject:_txtContactNum.text forKey:kmobile];
        [dicSignUp setObject:@(123) forKey:kdevicetoken];
        [dicSignUp setObject:@"ios" forKey:kisiosandroid];

        
        [AFNetworkManager postOperationServiceCallWithParameter:dicSignUp strURL:API_URL_With_(Mode_RegisterLogin) :^(id sucess)
         {
             
             if ([[sucess valueForKey:kErrorStatus]integerValue]==0) {
                 
                 NSDictionary *userDetail = [sucess valueForKey:kData];
                 NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:userDetail];
                 [[NSUserDefaults standardUserDefaults] setObject:personEncodedObject forKey:kData];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 
                 SetDefaultValue([[sucess valueForKey:kData] valueForKey:kuserid], kuserid);
                 ShareOBJ.userid = [GetDefaultValue(kuserid) integerValue];
                 
                 SetDefaultValue([[sucess valueForKey:kData] valueForKey:ktoken], kuserid);
                 ShareOBJ.token = GetDefaultValue(ktoken);


                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                     });
                 });
                 
                 [self performSelector:@selector(PushToHome) withObject:nil afterDelay:0.3];
                 
             }
             else
             {
                 [self AlertWithString:[sucess valueForKey:@"ErrorMessage"] withType:1];
                 
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                     });
                     
                 });
                 
             }
             
         } failure:^(NSError *Error) {
             NSLog(@"Fail at %@ ",Error);
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 // time-consuming task
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                 });
             });
             
             
         }];
    }

    
}
#pragma mark
#pragma mark -- TextFeild delegates and datasource

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
#pragma mark
#pragma mark --- cutom methods
-(IBAction)PopToHome:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)PushToHome
{
    UIStoryboard *tabStory  =[UIStoryboard storyboardWithName:@"TabStoryBord" bundle:nil];
    TabbarMainController *objTab = [tabStory instantiateViewControllerWithIdentifier:@"TabbarMainController"];
    
    
    [self.navigationController pushViewController:objTab animated:YES];
}
#pragma mark
#pragma mark --- CustomAlert
-(void)AlertWithString:(NSString*)strError withType :(NSInteger)alertType
{
    
    CWStatusBarNotification *notification = [CWStatusBarNotification new];
    if (alertType==0) {
        notification.notificationLabelBackgroundColor = [UIColor colorWithRed:195/255.0 green:66/255.0 blue:45/255.0 alpha:1];
    }
    else if(alertType==1)
    {
        notification.notificationLabelBackgroundColor = APPGREENCOLOR;
    }
    
    notification.notificationLabelTextColor = [UIColor whiteColor];
    notification.notificationLabelFont =[UIFont systemFontOfSize:12];
    
    notification.notificationAnimationInStyle =CWNotificationAnimationStyleTop;
    notification.notificationAnimationOutStyle =CWNotificationAnimationStyleTop;
    [notification displayNotificationWithMessage:strError
                                     forDuration:2.0f];

}
@end
