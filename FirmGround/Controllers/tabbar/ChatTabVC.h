//
//  ChatTabVC.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/22/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupListCell.h"
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <FirebaseAuth/FirebaseAuth.h>
#import "SVProgressHUD.h"
#import "MHPrettyDate.h"
@interface ChatTabVC : UIViewController<UITableViewDelegate,UITableViewDataSource>{

    FIRDatabaseReference *dbRefUsers;
    FIRDatabaseHandle dbRefHandle;
    FIRDatabaseHandle dbRefRemove;
    
    NSMutableArray *arrCurrentUserData;

}
@property (strong, nonatomic) IBOutlet UITableView *tblList;

@end
