//
//  ChatTabVC.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/22/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ChatTabVC.h"
#import "Constant.h"
#import "UIBarButtonItem+Extra.h"
#import "CreateGroupVCScreen.h"
@interface ChatTabVC ()
@property (assign, nonatomic) MHPrettyDateFormat   dateFormat;
@property (strong, nonatomic) NSCalendar*          calendar;

@end

@implementation ChatTabVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrCurrentUserData=[[NSMutableArray alloc]init];
    
    self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    
    self.dateFormat   = MHPrettyDateFormatWithTime;


//    UIImage *imgBackground = [[UIImage imageNamed:@"background"]stretchableImageWithLeftCapWidth:0.0
//                                                                                    topCapHeight:5.0];
//    UIImageView *imgviewBG =[[UIImageView alloc]initWithImage:imgBackground];
//    [imgviewBG setContentMode:UIViewContentModeScaleAspectFit];
//    [self.view addSubview:imgviewBG];
        
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      FONT_HELV_Medium(15),
      NSFontAttributeName,
      nil]];
    
    self.tabBarController.title = @"firmground";
    
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    UIImage *currentImgae =  [[UIImage imageNamed:@"gradinat"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [self.navigationController.navigationBar setBackgroundImage:currentImgae forBarMetrics:UIBarMetricsDefault];
    
//    UIButton* customButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
//    customButton.imageEdgeInsets = UIEdgeInsetsMake(0, 80, 0, 0);
//    customButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
//    [customButton setImage:[UIImage imageNamed:@"menu_chat_icn_sel"] forState:UIControlStateNormal];
//    [customButton setTitle:@"Chat" forState:UIControlStateNormal];
//    [customButton sizeToFit];
//    UIBarButtonItem* customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customButton];
//    self.navigationItem.leftBarButtonItem = customBarButtonItem;
    
    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Create Group" style:UIBarButtonItemStylePlain target:self action:@selector(btnToCreateGroup:)];
    self.tabBarController.navigationItem.rightBarButtonItem = backButtonItem;
    [backButtonItem barButtonDesingn];
    
    self.tabBarController.navigationItem.hidesBackButton = YES;
    
//    [_tblList registerClass:[GroupListCell class] forCellReuseIdentifier:@"GroupListCell"];
    
    _tblList.delegate = self;
    _tblList.dataSource = self;
    
    [SVProgressHUD showWithStatus:@"Please wait..." ];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    [_tblList setTableFooterView:[UIView new]];

    [self manageUChangedEventsOfDataBasedata:nil];


}
#pragma mark Manage Firebase Data

-(void)manageUChangedEventsOfDataBasedata:(NSDictionary*)dataDict{
    
//    if (dataDict==nil) {
//        
//        dbRefUsers=[[FIRDatabase database] referenceWithPath:@"threads"];
//    }
    
////    [[dbRefUsers childByAutoId]setValue:dataDict];
//    
//    dbRefHandle= [dbRefUsers observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        
//        NSDictionary *childAdded=snapshot.value;
//        NSString *message = [childAdded valueForKey:@"ChatMessage"];
//        
//        [arrCurrentUserData addObject:childAdded];
//        NSLog(@"%@",childAdded);
//        
//        if (dataDict==nil) {
//            //[self reloadTableWihMessage:message];
//        }
//    }];
//    
//    dbRefHandle= [dbRefUsers observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        
//        NSDictionary *childChanged=snapshot.value;
//        NSLog(@"child %@",childChanged);
//        
//    }];
    dbRefUsers = [[FIRDatabase database] referenceWithPath:@"FirmGroundDB"];


    [[dbRefUsers child:@"threads"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSEnumerator *children = [snapshot children];
        FIRDataSnapshot *child;
        while (child = [children nextObject]) {
            // ...
            [arrCurrentUserData addObject:child];
            
            }
        
        arrCurrentUserData=[[[arrCurrentUserData reverseObjectEnumerator] allObjects] mutableCopy];
        
        [SVProgressHUD dismiss];
        [_tblList reloadData];
        // ...
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"Error %@", error.localizedDescription);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark
#pragma mark - Tableview deleagte and datasource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
   return arrCurrentUserData.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupListCell" forIndexPath:indexPath];
    
    FIRDataSnapshot *ob = [arrCurrentUserData objectAtIndex:indexPath.row];
    
    NSLog(@"%@",ob.value[@"details"][@"GroupName"]);

    cell.lblName.text = ob.value[@"details"][@"GroupName"];
    cell.lbllastMessage.text = ob.value[@"details"][@"LastMessage"];
    cell.lblLastuserName.text = ob.value[@"details"][@"SenderNumber"];

    NSString *date1 = ob.value[@"details"][@"LastMessageTime"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([date1 integerValue] / 1000.0)];
    
    NSString *str = [MHPrettyDate prettyDateFromDate:date withFormat:self.dateFormat];

    cell.lblTime.text = str;

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    UIColor  *bordercolor = [cell InitwithCustumCellwithIndexpath:indexPath];

    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    Pushviewcontroller(@"MainCollectionVC");


}
#pragma mark
#pragma mark - UIbuttons Actions


-(IBAction)btnToCreateGroup:(id)sender{
    
    UIStoryboard *stor =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CreateGroupVCScreen *obj =[stor instantiateViewControllerWithIdentifier:kSICreateGroupVCScreen];
    
    [self.navigationController pushViewController:obj animated:YES];
    
    
}
@end
