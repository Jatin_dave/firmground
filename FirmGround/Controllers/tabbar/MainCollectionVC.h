//
//  MainCollectionVC.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAYCalendarView.h"

@import GooglePlacePicker;
@interface MainCollectionVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate, UIScrollViewDelegate,UITextFieldDelegate>
{
    GMSPlacePicker *_placePicker;
    
        UIToolbar *toolbar;
}
@property (weak, nonatomic) IBOutlet UICollectionView *mainCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *imgPageOne;
@property (weak, nonatomic) IBOutlet UIImageView *imgPageTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imgPageThree;
@property (strong, nonatomic) IBOutlet DAYCalendarView *calenderView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datepicker;
@end
