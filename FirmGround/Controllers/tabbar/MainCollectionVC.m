//
//  MainCollectionVC.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "MainCollectionVC.h"
#import "ConfirmGameCell.h"
#import "ChoosePlayersCell.h"
#import "ChatMessagesCell.h"
#import "Constant.h"
#import "Colors.h"
#import "GradientImage.h"
#import "DAYCalendarView.h"
#import "UIBarButtonItem+Extra.h"

@interface MainCollectionVC ()
{
    ConfirmGameCell *confirmGameCell;
    ChatMessagesCell *chatMessagesCell;
    ChoosePlayersCell *choosePlayersCell;
}

@end

@implementation MainCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor clearColor];
    _mainCollectionView.backgroundColor=[UIColor clearColor];
    
    _mainCollectionView.contentOffset = CGPointMake(_mainCollectionView.frame.size.width, 0);
    
    
    [self setupNavigationBar];
    
    [self setCalenderView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = YES;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupNavigationBar{
    
    
    

    self.navigationItem.hidesBackButton = YES;
    
    self.title=@"Sunday Foottie";
    
    UIImage *currentImgae =  [[UIImage imageNamed:@"gradinat"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [self.navigationController.navigationBar setBackgroundImage:currentImgae forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      FONT_HELV_Medium(15),
      NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,
      nil]];

    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};

    //set right button
    
//    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Create Group" style:UIBarButtonItemStylePlain target:self action:@selector(createGroup:)];
//    self.tabBarController.navigationItem.rightBarButtonItem = backButtonItem;
//    [backButtonItem barButtonDesingn];
//
//    [self.navigationItem setRightBarButtonItem:backButtonItem];
//    
//    //set left button
//    
//    UIButton *btnChat = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 60)];
//    [btnChat setTitle:@"Chat" forState:UIControlStateNormal];
//    btnChat.titleLabel.font=FONT_HELV_LIGHT(11);
//    [btnChat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btnChat setImage:[UIImage imageNamed:@"menu_chat_icn"] forState:UIControlStateNormal];
//    [btnChat setImageEdgeInsets:UIEdgeInsetsMake(0,-50, 0, 0)];
//    [btnChat setTitleEdgeInsets:UIEdgeInsetsMake(0, -50, 0, 0)];
//    [btnChat addTarget:self action:@selector(btnChatAction:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:btnChat];
//    [self.navigationItem setLeftBarButtonItem:leftButton];
    
    UIImage *faceImage = [UIImage imageNamed:@"iconback"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    [face addTarget:self action:@selector(PopToView:) forControlEvents:UIControlEventTouchUpInside];
    face.bounds = CGRectMake( 0, 0, faceImage.size.width, faceImage.size.height );
    [face setImage:faceImage forState:UIControlStateNormal];
    UIBarButtonItem *LEftButton = [[UIBarButtonItem alloc] initWithCustomView:face];
    [self.navigationItem setLeftBarButtonItem:LEftButton];
    

}


-(IBAction)PopToView:(id)sender{
   
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)btnChatAction:(id)sender{
    
}
-(void)setCalenderView{
    confirmGameCell.txtCalender.text=@"02/02/02017";
    self.calenderView.showUserEvents =YES;
    self.calenderView.singleRowMode = NO;
    [_calenderView addTarget:self action:@selector(CalanderDateChanged) forControlEvents:UIControlEventValueChanged];

}

#pragma mark collectionviewMethods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.item==0) {
        
        choosePlayersCell =[collectionView dequeueReusableCellWithReuseIdentifier:@"choosePlayersCell" forIndexPath:indexPath];
        [choosePlayersCell.tableIn reloadData];
        [choosePlayersCell.tableOut reloadData];

        return choosePlayersCell;
    }
    else if (indexPath.item==1)
    {
        chatMessagesCell =[collectionView dequeueReusableCellWithReuseIdentifier:@"chatMessagesCell" forIndexPath:indexPath];
        [chatMessagesCell.chatTableView reloadData];

        return chatMessagesCell;
        
    }else if (indexPath.item==2)
    {
        confirmGameCell =[collectionView dequeueReusableCellWithReuseIdentifier:@"confirmGameCell" forIndexPath:indexPath];
        return confirmGameCell;
        
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.frame.size;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
    if (scrollView.contentOffset.x==0) {
        _imgPageOne.image=[UIImage imageNamed:@"slider_circle_active_icn"];
        _imgPageTwo.image=[UIImage imageNamed:@"slider_circle_unactive_icn"];
        _imgPageThree.image=[UIImage imageNamed:@"slider_circle_unactive_icn"];
        [chatMessagesCell textKeybordClosedAnimated:NO];
        [self.view endEditing:YES];
        
    }
    else if (scrollView.contentOffset.x ==  _mainCollectionView.frame.size.width){
        _imgPageTwo.image=[UIImage imageNamed:@"slider_circle_active_icn"];
        _imgPageOne.image=[UIImage imageNamed:@"slider_circle_unactive_icn"];
        _imgPageThree.image=[UIImage imageNamed:@"slider_circle_unactive_icn"];
        [self.view endEditing:YES];
        [chatMessagesCell textKeybordClosedAnimated:NO];
    }
    else if (scrollView.contentOffset.x==_mainCollectionView.frame.size.width*2){
        _imgPageThree.image=[UIImage imageNamed:@"slider_circle_active_icn"];
        _imgPageTwo.image=[UIImage imageNamed:@"slider_circle_unactive_icn"];
        _imgPageOne.image=[UIImage imageNamed:@"slider_circle_unactive_icn"];
        [chatMessagesCell textKeybordClosedAnimated:NO];
        [self.view endEditing:YES];

        
    }
    [_mainCollectionView reloadData];
}
#pragma mark
#pragma mark ----- UIDatepicker
-(IBAction)btnToSelectDate:(id)sender{
    
    
}
#pragma mark
#pragma mark -- TextFeild delegates and datasource

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == confirmGameCell.txttime) {
        
        _datepicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,SCREEN_HEIGHT-216, SCREEN_WIDTH, 216)];
        [_datepicker setDatePickerMode:UIDatePickerModeTime];
        [_datepicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        _datepicker.backgroundColor =[UIColor colorWithPatternImage:GRADIENT_IMG];

        toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
        toolbar.barStyle = UIBarStyleDefault;
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
        [doneButton setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:BLACK_COLOR, NSForegroundColorAttributeName,nil]
                              forState:UIControlStateNormal];
        
        toolbar.items = @[flexibleSpaceLeft,doneButton];
        confirmGameCell.txttime.inputAccessoryView = toolbar;
        confirmGameCell.txttime.inputView = _datepicker;

        return YES;
    }
    
    else if (textField==confirmGameCell.txtCalender){
        
      //  _calenderView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,SCREEN_HEIGHT-216, SCREEN_WIDTH, 216)];
      //  [_calenderView setDatePickerMode:UIDatePickerModeTime];
      //  [_calenderView addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        _calenderView.backgroundColor =[UIColor colorWithPatternImage:GRADIENT_IMG];
        
        
        toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
        toolbar.barStyle = UIBarStyleDefault;
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissCalender)];
        [doneButton setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:BLACK_COLOR, NSForegroundColorAttributeName,nil]
                                  forState:UIControlStateNormal];
        
        toolbar.items = @[flexibleSpaceLeft,doneButton];
        confirmGameCell.txtCalender.inputAccessoryView = toolbar;
        confirmGameCell.txtCalender.inputView = _calenderView;
        
    }
    
    else if (textField == confirmGameCell.txtLocation)
    {
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(37.788204, -122.411937);
        CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
                                                                      center.longitude + 0.001);
        CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
                                                                      center.longitude - 0.001);
        GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                             coordinate:southWest];
        GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
        _placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
        
        [_placePicker pickPlaceWithCallback:^(GMSPlace *place, NSError *error) {
            if (error != nil) {
                NSLog(@"Pick Place error %@", [error localizedDescription]);
                return;
            }
            
            if (place != nil) {
                
                confirmGameCell.txtLocation.text = [[place.formattedAddress
                                      componentsSeparatedByString:@", "] componentsJoinedByString:@"\n"];
                
            } else {
//                self.nameLabel.text = @"No place selected";
//                self.addressLabel.text = @"";
                confirmGameCell.txtLocation.text = @"No place selected";
                
            }
        }];

    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}
-(void)dismiss
{
    [confirmGameCell.txttime resignFirstResponder];
}
- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    dateFormat.dateStyle = NSDateFormatterMediumStyle;
    NSString *dateString =  [dateFormat stringFromDate:datePicker.date];
    confirmGameCell.txttime.text = dateString;
    
    
}
#pragma mark calender value change

-(void)CalanderDateChanged{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd/MM/YYYY";
    NSLog(@"%@", [formatter stringFromDate:self.calenderView.selectedDate]);
    NSString *strDate=[formatter stringFromDate:self.calenderView.selectedDate];
    confirmGameCell.txtCalender.text=strDate;
}

-(void)dismissCalender{
    [confirmGameCell.txtCalender resignFirstResponder];
}
@end
