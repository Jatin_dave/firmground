//
//  ProfileTBScreen.h
//  FirmGround
//
//  Created by Admin on 22/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CWStatusBarNotification.h"
#import "DAYCalendarView.h"
@import GooglePlacePicker;
 



@interface ProfileTBScreen : UITableViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UIButton *btnEditBGPic;
     GMSPlacePicker *_placePicker;
    __weak IBOutlet UIImageView *imgBackground;
    IBOutlet UIView *viewBottom;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UITableView *tblList;
    
    NSString *strImage;
    UIImagePickerController *imgPickerView;
    BOOL isBackgroundPic;

}
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet DAYCalendarView *calendarView;
@property (strong, nonatomic) IBOutlet UIView *viewCalendarBack;

@property (strong, nonatomic) IBOutlet UIButton *btnTakePic;
@property (strong, nonatomic) IBOutlet UITextField *txtname;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtDob;
@property (strong, nonatomic) IBOutlet UITextField *txtNum;
@property (strong, nonatomic) IBOutlet UITextField *txtLocation;
@property (strong, nonatomic) IBOutlet UITextField *txtJoin;

@property (strong, nonatomic) IBOutlet UITextField *txtEMname;
@property (strong, nonatomic) IBOutlet UITextField *txtEMNum
;
@end
