//
//  ProfileTBScreen.m
//  FirmGround
//
//  Created by Admin on 22/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ProfileTBScreen.h"
#import "Constant.h"
#import "SVProgressHUD.h"
#import "AFNetworkManager.h"
#import "User.h"
#import "UITextField+Extra.h"
#import "Validation.h"
@interface ProfileTBScreen ()

@end

@implementation ProfileTBScreen
#pragma mark
#pragma mark - LifeCycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *imgBackground = [[UIImage imageNamed:@"background"]stretchableImageWithLeftCapWidth:0.0
                                                                                    topCapHeight:5.0];
    UIImageView *imgviewBG =[[UIImageView alloc]initWithImage:imgBackground];
    self.tableView.backgroundView = imgviewBG;
    
    [_txtname setDefaultTextFieldStyleForUserName:@"Username"];
    [_txtEmail setDefaultTextFieldStyleForUserName:@"Email"];
    [_txtDob setDefaultTextFieldStyleForUserName:@"DOB"];
    [_txtNum setDefaultTextFieldStyleForUserName:@"Number"];
    [_txtLocation setDefaultTextFieldStyleForUserName:@"Loation"];
    [_txtJoin setDefaultTextFieldStyleForUserName:@"Join date"];
    [_txtEMname setDefaultTextFieldStyleForUserNamewithPadingNO:@"emergency name"];
    [_txtEMNum setDefaultTextFieldStyleForUserNamewithPadingNO:@"emergency Numver"];
    
    _txtDob.delegate = self;
    _txtLocation.delegate = self;
    
    [btnEditBGPic setHidden:YES];

    [self txtFeildEnable:NO];
    
    tblList.bounces = NO;
    
    [tblList setContentInset:UIEdgeInsetsMake(0, 0, 50, 0)];
    
    // set current date in textfield
  //  [_txtDob setPlaceholder:[self getPlaceholderDate]];
    
    [self.calendarView addTarget:self action:@selector(calendarViewDidChange:) forControlEvents:UIControlEventValueChanged];
    self.calendarView.boldPrimaryComponentText = YES;
    self.calendarView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
    [self.calendarView reloadViewAnimated:YES];
    [self.calendarView setSelectedDate:[self getSelectedDate]];
    
    [_viewCalendarBack setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2)];
    [_txtDob setInputView:_viewCalendarBack];
    [_txtDob setTintColor:[UIColor clearColor]];


}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
        [SVProgressHUD showWithStatus:@"Please wait..." ];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        
        NSMutableDictionary *dicSignUp =[[NSMutableDictionary alloc]init];
        
        [dicSignUp setObject:@(ShareOBJ.userid) forKey:kuserid];
        [dicSignUp setObject:ShareOBJ.token forKey:ktoken];

        [AFNetworkManager postOperationServiceCallWithParameter:dicSignUp strURL:API_URL_With_(Mode_GetProfile) :^(id sucess)
         {
             
             if ([[sucess valueForKey:kErrorStatus]integerValue]==0) {
                 
                 
                 User *objUser = [[User alloc]initWithObject:[sucess valueForKey:kData]];;
                 
                 _txtname.text = objUser.name;
                 _txtEmail.text = objUser.username;
                 _txtDob.text = objUser.dob;
                 _txtNum.text = objUser.mobile;
                 _txtLocation.text = objUser.location;
                 _txtJoin.text = objUser.createddate;
                 _txtEMname.text = objUser.emergencyname;
                 _txtEMNum.text = objUser.emergencymobile;


                 
                 NSLog(@"%ld",(long)objUser.userid);
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                     });
                 });
                 
                 
             }
             else
             {
                 
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                     });
                     
                 });
                 
             }
             
         } failure:^(NSError *Error) {
             NSLog(@"Fail at %@ ",Error);
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 // time-consuming task
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                 });
             });
             
             
         }];
    
}
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.layer.masksToBounds = YES;
    imgProfile.layer.borderWidth = 6;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    
    viewBottom.layer.cornerRadius = 20;
    viewBottom.layer.masksToBounds = YES;
    viewBottom.layer.borderWidth = 2;
    viewBottom.layer.borderColor = GET_COLOR_WITH_RGB(223, 223, 223, 1).CGColor;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - UIbuttons Actions
- (IBAction)btnCloseTap:(id)sender {
    //    [UIView animateWithDuration:0.5 animations:^{
    //        [_viewCalendarBack setFrame:CGRectMake(0,SCREENHEIGHT, SCREENWIDTH, 450)];
    //
    //    }completion:^(BOOL finished) {
    //        [_viewCalendarBack removeFromSuperview];
    //
    //    }];
    
    [_txtDob resignFirstResponder];
}
- (IBAction)btneditBackgroundPic:(UIButton *)sender {
    
    
    isBackgroundPic = YES;
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:APP_NAME message:@"Choose Background Picture!" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            imgPickerView = [[UIImagePickerController alloc] init];
            imgPickerView.delegate = self;
            imgPickerView.sourceType =UIImagePickerControllerSourceTypeCamera;
            imgPickerView.cameraDevice =UIImagePickerControllerCameraDeviceFront;
            imgPickerView.allowsEditing = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imgPickerView animated:YES completion:nil];
                
            });
            //   [imgPickerView popoverPresentationController];
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            imgPickerView = [[UIImagePickerController alloc] init];
            imgPickerView.delegate = self;
            [imgPickerView setAllowsEditing:YES];
            imgPickerView.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self presentViewController:imgPickerView animated:YES completion:nil];
            });
        }
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(IBAction)btnToChoosePic:(id)sender{
    
    isBackgroundPic = NO;
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:APP_NAME message:@"Choose Picture!" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            imgPickerView = [[UIImagePickerController alloc] init];
            imgPickerView.delegate = self;
            imgPickerView.sourceType =UIImagePickerControllerSourceTypeCamera;
            imgPickerView.cameraDevice =UIImagePickerControllerCameraDeviceFront;
            imgPickerView.allowsEditing = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:imgPickerView animated:YES completion:nil];
                
            });
            //   [imgPickerView popoverPresentationController];
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            imgPickerView = [[UIImagePickerController alloc] init];
            imgPickerView.delegate = self;
            [imgPickerView setAllowsEditing:YES];
            imgPickerView.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self presentViewController:imgPickerView animated:YES completion:nil];
            });
        }
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
    }]];
   
    [self presentViewController:actionSheet animated:YES completion:nil];

}
-(IBAction)btnEditProfile:(UIButton *)sender{
    
    if (sender.isSelected) {
        
        if (![Validation textFieldLength:_txtname.text]) {
            
            [self AlertWithString:@"Please Enter name" withType:0];
            
        }
        else if (![Validation textFieldLength:_txtEmail.text]) {
            
            [self AlertWithString:@"Please Enter Email" withType:0];
            
        }
        else if (![Validation validateEmail:_txtEmail.text])
        {
            [self AlertWithString:@"Please Enter Valid Email" withType:0];
            
        }
        else if (![Validation textFieldLength:_txtDob.text])
        {
            [self AlertWithString:@"Please Enter Date of Birth" withType:0];
            
        }
        else if (![Validation textFieldLength:_txtNum.text])
        {
            [self AlertWithString:@"Please Enter Contack No" withType:0];
            
        }
        else if (![Validation textFieldLength:_txtLocation.text])
        {
            [self AlertWithString:@"Please Select Loaction" withType:0];
            
        }
        else if (![Validation textFieldLength:_txtEMname.text])
        {
            [self AlertWithString:@"Please Enter Emergency Name" withType:0];
            
        }
        else if (![Validation textFieldLength:_txtEMNum.text])
        {
            [self AlertWithString:@"Please Enter Emergency Contact No" withType:0];
            
        }
        else{
            
            [SVProgressHUD showWithStatus:@"Please wait..." ];
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
            
            NSMutableDictionary *dicSignUp =[[NSMutableDictionary alloc]init];
            
            [dicSignUp setObject:@(ShareOBJ.userid) forKey:kuserid];
            [dicSignUp setObject:ShareOBJ.token forKey:ktoken];
            [dicSignUp setObject:_txtname.text forKey:kname];
            [dicSignUp setObject:_txtEmail.text forKey:kusername];
            [dicSignUp setObject:_txtDob.text forKey:kdob];
            [dicSignUp setObject:_txtLocation.text forKey:klocation];
            [dicSignUp setObject:_txtNum.text forKey:kmobile];
            [dicSignUp setObject:_txtEMname.text forKey:kename];
            [dicSignUp setObject:_txtEMNum.text forKey:kemobile];
            
            
            [AFNetworkManager postOperationServiceCallWithParameter:dicSignUp strURL:API_URL_With_(Mode_UpdateProfile) :^(id sucess)
             {
                 
                 if ([[sucess valueForKey:kErrorStatus]integerValue]==0) {
                     
                     [sender setSelected:NO];
                     [btnEditBGPic setHidden:YES];
                     User *objUser = [[User alloc]initWithObject:[sucess valueForKey:kData]];;
                     
                     _txtname.text = objUser.name;
                     _txtEmail.text = objUser.username;
                     _txtDob.text = objUser.dob;
                     _txtNum.text = objUser.mobile;
                     _txtLocation.text = objUser.location;
                     _txtJoin.text = objUser.createddate;
                     _txtEMname.text = objUser.emergencyname;
                     _txtEMNum.text = objUser.emergencymobile;
                     
                     [self txtFeildEnable:NO];
                     
                     NSLog(@"%ld",(long)objUser.userid);
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [SVProgressHUD dismiss];
                             
                             [self profileImageUpload];
                         });
                     });
                     
                     [self AlertWithString:@"profile updated successfully" withType:1];

                 }
                 else
                 {
                     
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         // time-consuming task
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [SVProgressHUD dismiss];
                             [self AlertWithString:[sucess valueForKey:@"ErrorMessage"] withType:1];

                             [self txtFeildEnable:YES];
                             
                         });
                         
                     });
                     
                 }
                 
             } failure:^(NSError *Error) {
                 NSLog(@"Fail at %@ ",Error);
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     // time-consuming task
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [SVProgressHUD dismiss];
                         [self txtFeildEnable:YES];
                         
                     });
                 });
                 
             }];

        }
    

    }else{
        [sender setSelected:YES];
        [btnEditBGPic setHidden:NO];
        [self txtFeildEnable:YES];

    }
}
#pragma mark
#pragma mark -- TextFeild delegates and datasource

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == _txtLocation) {
        
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(37.788204, -122.411937);
        CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
                                                                      center.longitude + 0.001);
        CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
                                                                      center.longitude - 0.001);
        GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                             coordinate:southWest];
        GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
        _placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
        
        [_placePicker pickPlaceWithCallback:^(GMSPlace *place, NSError *error) {
            if (error != nil) {
                NSLog(@"Pick Place error %@", [error localizedDescription]);
                return;
            }
            
            if (place != nil) {
                
                _txtLocation.text = [[place.formattedAddress
                                           componentsSeparatedByString:@", "] componentsJoinedByString:@"\n"];
                
            } else {
                self.nameLabel.text = @"No place selected";
                self.addressLabel.text = @"";
                _txtLocation.text = @"No place selected";

            }
        }];

        return NO;
    }
    return YES;
}

#pragma mark
#pragma mark ---- Calender Delegates
#pragma mark
#pragma mark - Create formatted date string
-(IBAction)calendarViewDidChange:(DAYCalendarView *)sender
{
    NSString *strDate = [NSString stringWithFormat:@"%@",sender.selectedDate];
    
    //2016-08-07 18:30:00 +0000
    [self setDateFromCalenderDate:strDate];
}

-(NSString *)getPlaceholderDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    
    //    return [dateFormatter stringFromDate:[NSDate date]];
    
    int secondsInOneDay = 60*60*24;
    int numberOfDays = 2;
    NSDate *dateSelectable =  [[NSDate date] dateByAddingTimeInterval:(secondsInOneDay*numberOfDays)];
    
    return [dateFormatter stringFromDate:dateSelectable];
}

-(NSDate *)getSelectedDate
{
    NSDateFormatter *dateFormatterStore = [[NSDateFormatter alloc] init];
    [dateFormatterStore setDateFormat:@"yyyy-MM-dd HH:mm:ssZ"];
    
    int secondsInOneDay = 60*60*24;
    int numberOfDays = 2;
    return [[NSDate date] dateByAddingTimeInterval:(secondsInOneDay*numberOfDays)];
}


-(void)setDateFromCalenderDate:(NSString *)strCalenderDate
{
    
    NSDateFormatter *dateFormatterStore = [[NSDateFormatter alloc] init];
    [dateFormatterStore setDateFormat:@"yyyy-MM-dd HH:mm:ssZ"];
    NSDate *dateSession = [dateFormatterStore dateFromString:strCalenderDate];
    [dateFormatterStore  setDateFormat:@"yyyy-MM-dd"];
    
    NSString *strDateSesssion = [dateFormatterStore stringFromDate:dateSession];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ssZ"];
    NSDate *date  = [dateFormatter dateFromString:strCalenderDate];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    NSString *newDate = [dateFormatter stringFromDate:date];
    _txtDob.text=newDate;
    
    NSDateFormatter *dateFormatterForDay = [[NSDateFormatter alloc] init];
    // Convert to new Date Format
    [dateFormatterForDay setDateFormat:@"EEEE"];
    NSString *strDay=[dateFormatterForDay stringFromDate:date];
    
    if ([strDay isEqualToString:@"Sunday"]) {
        //weekId=1;
    }
    else if ([strDay isEqualToString:@"Monday"]) {
        //weekId=2;
    }
    else if ([strDay isEqualToString:@"Tuesday"]) {
        //weekId=3;
    }
    else if ([strDay isEqualToString:@"Wednesday"]) {
        //weekId=4;
    }
    else if ([strDay isEqualToString:@"Thursday"]) {
        //weekId=5;
    }
    else if ([strDay isEqualToString:@"Friday"]) {
        //weekId=6;
    }
    else if ([strDay isEqualToString:@"Saturday"]) {
        //weekId=7;
    }
    //NSLog(@"%@",ShareOBJ.BookedDay);
}


#pragma mark
#pragma mark --- Image Upload API
-(void)profileImageUpload
{
    
    [SVProgressHUD showWithStatus:@"Please wait..." ];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    NSMutableDictionary *dicSignUp =[[NSMutableDictionary alloc]init];
    
    [dicSignUp setObject:@(ShareOBJ.userid) forKey:kuserid];
    [dicSignUp setObject:ShareOBJ.token forKey:ktoken];
    [dicSignUp setObject:strImage forKey:kimageurl];
    
    [AFNetworkManager postOperationServiceCallWithParameter:dicSignUp strURL:API_URL_With_(Mode_UpdateImage) :^(id sucess)
     {
         
         if ([[sucess valueForKey:kErrorStatus]integerValue]==0) {
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 // time-consuming task
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                 });
             });
         }
         else
         {
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 // time-consuming task
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [SVProgressHUD dismiss];
                 });
             });
         }
         
     } failure:^(NSError *Error) {
         NSLog(@"Fail at %@ ",Error);
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             // time-consuming task
             dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD dismiss];
                 [self txtFeildEnable:YES];
             });
         });
     }];

}
#pragma mark
#pragma mark --- Imagepcikerview delegates
-(void)imagePickerController:(UIImagePickerController *)picker
      didFinishPickingImage : (UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
        UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
        [image drawInRect:(CGRect){0, 0, image.size}];
        UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        int maxFileSize = 250*720;
        
        NSData *imageData = UIImageJPEGRepresentation(normalizedImage, compression);
        
        while ([imageData length] > maxFileSize && compression > maxCompression)
        {
            compression -= 0.1;
            imageData = UIImageJPEGRepresentation(normalizedImage, compression);
        }
        strImage = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        
        //        [_btnProfilePic setImage:image forState:UIControlStateNormal];
        imgProfile.contentMode = UIViewContentModeScaleAspectFill;
        imgProfile.clipsToBounds = YES;
        
        
        if (isBackgroundPic) {
            
            [imgBackground setImage:normalizedImage];

        }else{
            [imgProfile setImage:normalizedImage];

        }
        
        [self profileImageUpload];
        
    }];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)  picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Table view data source
#pragma mark
#pragma mark - Custom methods

-(void)txtFeildEnable : (BOOL )Value
{
    [_txtname setEnabled:Value];
    [_txtEmail setEnabled:Value];
    [_txtDob setEnabled:Value];
    [_txtNum setEnabled:Value];
    [_txtLocation setEnabled:Value];
    [_txtJoin setEnabled:Value];
    [_txtEMname setEnabled:Value];
    [_txtEMNum setEnabled:Value];

}
#pragma mark
#pragma mark --- CustomAlert
-(void)AlertWithString:(NSString*)strError withType :(NSInteger)alertType
{
    
    CWStatusBarNotification *notification = [CWStatusBarNotification new];
    if (alertType==0) {
        notification.notificationLabelBackgroundColor = [UIColor colorWithRed:195/255.0 green:66/255.0 blue:45/255.0 alpha:1];
    }
    else if(alertType==1)
    {
        notification.notificationLabelBackgroundColor = APPGREENCOLOR;
    }
    
    notification.notificationLabelTextColor = [UIColor whiteColor];
    //    notification.notificationLabelFont =FONT_HELV_LIGHT(12);
    notification.notificationAnimationInStyle =CWNotificationAnimationStyleTop;
    notification.notificationAnimationOutStyle =CWNotificationAnimationStyleTop;
    [notification displayNotificationWithMessage:strError
                                     forDuration:2.0f];
    
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
