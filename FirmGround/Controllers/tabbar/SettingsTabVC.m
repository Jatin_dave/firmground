//
//  SettingsTabVC.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/22/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "SettingsTabVC.h"
#import "LoginTBScreen.h"
#import "AppDelegate.h"
#import "Constant.h"
@interface SettingsTabVC ()

@end

@implementation SettingsTabVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnlogout:(UIButton *)sender {
    
  
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:APP_NAME
                                 message:@"Are You Sure Want to Logout!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"No"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    
                                   

                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Logout"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   UIStoryboard *stor =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                   
                                   //            LoginTBScreen *objLoginScreen = [stor instantiateViewControllerWithIdentifier:@"LoginTBScreen"];
                                   
                                   UINavigationController *objLoginScreen =(UINavigationController *) [stor instantiateViewControllerWithIdentifier:@"NavMain"];
                                   
                                   
                                   AppDelegate *obj =(AppDelegate *) [[UIApplication sharedApplication] delegate];
                                   obj.window.rootViewController = objLoginScreen;
                               
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
