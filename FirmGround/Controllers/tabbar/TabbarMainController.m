//
//  TabbarMainController.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/22/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TabbarMainController.h"
#import "ChatTabVC.h"
#import "LiveFeedTabVC.h"
#import "ProfileTBScreen.h"
#import "SettingsTabVC.h"
#import "SocialTabVC.h"

@implementation TabbarMainController

-(void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ChatTabVC *chatVc=[self.storyboard instantiateViewControllerWithIdentifier:@"ChatTabVC"];
    LiveFeedTabVC *liveVc=[self.storyboard instantiateViewControllerWithIdentifier:@"LiveFeedTabVC"];
    ProfileTBScreen *profVc=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileTBScreen"];
    SettingsTabVC *settingVc=[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsTabVC"];
    SocialTabVC *socVc=[self.storyboard instantiateViewControllerWithIdentifier:@"SocialTabVC"];
    
    
    NSMutableArray *tabbarViewControllers=[[NSMutableArray alloc]init];
  
    
    chatVc.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Chat" image:[UIImage imageNamed:@"menu_chat_icn"] tag:1];
    
//    UINavigationController *firstNavController = [[UINavigationController alloc]initWithRootViewController:chatVc];
   // firstNavController.title = @"FirmGround";

    profVc.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Profile" image:[UIImage imageNamed:@"menu_profile_icn"] tag:2];
    socVc.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Social" image:[UIImage imageNamed:@"menu_social_icn"] tag:3];
    liveVc.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Live Feed" image:[UIImage imageNamed:@"menu_feed_icn"] tag:4];
    settingVc.tabBarItem=[[UITabBarItem alloc]initWithTitle:@"Settings" image:[UIImage imageNamed:@"menu_settings_icn"] tag:5];
    
    [tabbarViewControllers addObject:chatVc];
    [tabbarViewControllers addObject:profVc];
    [tabbarViewControllers addObject:socVc];
    [tabbarViewControllers addObject:liveVc];
    [tabbarViewControllers addObject:settingVc];

    [self setViewControllers:tabbarViewControllers];

    [self setSelectedViewController:[tabbarViewControllers objectAtIndex:0]];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
    switch (item.tag) {
        case 1:
            
            break;
            
        case 2:
            
            
            break;
        case 3:
            
            
            break;
        case 4:
            
            
            break;
        case 5:
            
            
            break;
            
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

@end
