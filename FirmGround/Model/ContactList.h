//
//  ContactList.h
//  FirmGround
//
//  Created by Admin on 06/08/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ContactList : NSObject

@property (nonatomic,strong) NSString *phoneNumber;
@property (nonatomic,strong) NSString *givenName;
@property (nonatomic,strong) NSString *familyName;
@property (nonatomic,strong) NSString *emailAddresses;
@property (nonatomic,assign) NSInteger isSelected;
@property (nonatomic,strong) UIColor *borderClor;

-(id)initWithObject :(NSMutableDictionary *)object;

@end
