//
//  ContactList.m
//  FirmGround
//
//  Created by Admin on 06/08/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ContactList.h"
#import "Constant.h"

@implementation ContactList
@synthesize phoneNumber = _phoneNumber;
@synthesize emailAddresses = _emailAddresses;
@synthesize givenName = _givenName;
@synthesize familyName = _familyName;
@synthesize isSelected = _isSelected;

-(id)initWithObject :(NSMutableDictionary *)object
{
    self = [[[self class] alloc] init];
    if (self)
    {
        @try
        {
            
            [self setPhoneNumber:[object valueForKey:kphoneNumber]];
            [self setEmailAddresses:[object valueForKey:kemailAddresses]];
            [self setGivenName:[object valueForKey:kgivenName]];
            [self setFamilyName:[object valueForKey:kfamilyName]];
            [self setIsSelected:[[object valueForKey:kisSelected] integerValue]];

        }
        @catch (NSException *exception)
        {
            LOG_EXCEPTION(exception);
        }
        @finally {
        }
        
    }
    return self;
}
@end
