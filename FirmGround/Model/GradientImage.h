//
//  GradientImage.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/30/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface GradientImage : NSObject


+(UIImage*)getGradientImageWithStartColor:(UIColor*)startColor endColor:(UIColor*)endColor;


@end
