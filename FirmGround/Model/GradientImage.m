//
//  GradientImage.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/30/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "GradientImage.h"
#import "Colors.h"
@implementation GradientImage



+(UIImage*)getGradientImageWithStartColor:(UIColor*)startColor endColor:(UIColor*)endColor{
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(0, 0, 414, 60);
    gradientLayer.colors = @[ (__bridge id)startColor.CGColor,
                              (__bridge id)endColor.CGColor ];
    gradientLayer.startPoint = CGPointMake(0.0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    
    
    UIGraphicsBeginImageContext(gradientLayer.bounds.size);
    [gradientLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *gradientImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return gradientImage;
}

@end
