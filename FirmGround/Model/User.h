//
//  User.h
//  gubber
//
//  Created by Admin on 11/09/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import <UIKit/UIKit.h>

@interface User : NSObject

@property (nonatomic,strong) NSString *createddate;

@property (nonatomic,strong) NSString *dob;
@property (nonatomic,strong) NSString *emergencymobile;
@property (nonatomic,strong) NSString *emergencyname;
@property (nonatomic,strong) NSString *location;
@property (nonatomic,strong) NSString *name;
@property (assign) NSInteger userid;
@property (nonatomic,strong) NSString *mobile;
@property (nonatomic,strong) NSString *username;
@property (nonatomic,assign) NSInteger isSelected;
@property (nonatomic,strong) UIColor *borderClor;



-(id)initWithObject :(NSMutableDictionary *)object;
@end
