//
//  User.m
//  gubber
//
//  Created by Admin on 11/09/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "User.h"
@implementation User

@synthesize createddate = createddate;
@synthesize dob = _dob;
@synthesize emergencymobile = _emergencymobile;
@synthesize emergencyname = _emergencyname;
@synthesize location = _location;
@synthesize name = _name;
@synthesize userid = _userid;
@synthesize mobile = _mobile;
@synthesize username = _username;
@synthesize isSelected = _isSelected;
@synthesize borderClor = _borderClor;


-(id)initWithObject :(NSMutableDictionary *)object
{
    self = [[[self class] alloc] init];
    if (self)
    {
        @try
        {
          
            [self setName:[object valueForKey:kname]];
            [self setCreateddate:[object valueForKey:kcreateddate]];
            [self setDob:[object valueForKey:kdob]];
            [self setEmergencyname:[object valueForKey:kemergencyname]];
            [self setEmergencymobile:[object valueForKey:kemergencymobile]];
            [self setLocation:[object valueForKey:klocation]];
            [self setMobile:[object valueForKey:kmobile]];
            [self setUsername:[object valueForKey:kusername]];
            
            [self setIsSelected:[[object valueForKey:kisSelected] integerValue]];
            [self setUserid:[[object valueForKey:kuserid] integerValue]];

            
        }
        @catch (NSException *exception)
        {
            LOG_EXCEPTION(exception);
        }
        @finally {
        }
        
    }
    return self;
}

@end
