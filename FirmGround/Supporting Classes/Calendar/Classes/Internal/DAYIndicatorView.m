//
//  DAYIndicatorView.m
//  Daysquare
//
//  Created by 杨弘宇 on 16/6/7.
//  Copyright © 2016年 Cyandev. All rights reserved.
//

#import "DAYIndicatorView.h"
#import "DAYUtils.h"


@interface DAYIndicatorView ()

@property (strong, nonatomic) CAShapeLayer *ellipseLayer;

@end

@implementation DAYIndicatorView

- (void)didMoveToSuperview {
    [super didMoveToSuperview];


    
    self.transform = CGAffineTransformMakeScale(-0.7, -0.7);
    self.ellipseLayer = [CAShapeLayer layer];
    self.ellipseLayer.fillColor = self.color.CGColor;
    [self.layer addSublayer:self.ellipseLayer];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.transform = CGAffineTransformMakeScale(-0.7, -0.7);
    self.ellipseLayer.path = CGPathCreateWithEllipseInRect(self.bounds, nil);
    self.ellipseLayer.frame = self.bounds;
}

- (void)setColor:(UIColor *)color {
    self.transform = CGAffineTransformMakeScale(-0.7, -0.7);

    self->_color = color;
    self.ellipseLayer.fillColor = color.CGColor;
}

@end
