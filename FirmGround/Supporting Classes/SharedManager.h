//
//  SharedManager.h
//  BNE
//
//  Created by Maurya on 03/02/15.
//  Copyright (c) 2015 i-verve infoWeb. All rights reserved.
//

#import <Foundation/Foundation.h>
@class  InCaseEmergency;
//@class Patient;
//@class Doctor;
//@class Ambulance;

@interface SharedManager : NSObject

@property(assign,readwrite)  BOOL isNetAvailable;
@property (retain, nonatomic) NSString *deviceToken;
@property(retain,nonatomic) NSString *deviceType,*token;
@property (retain,nonatomic) NSString *instagramAccessToken,*strdes;
@property (retain,nonatomic) NSMutableArray *arrUserList,*arrCardList,*arrSelectedPlateFrom;
@property (assign) long long int  TotalLikesOfUser;
@property (retain,nonatomic) NSString *strMediaId;

@property (assign) BOOL isAlertTypeCheck;
@property (assign) BOOL isFirstTImeLogin;
@property (assign) BOOL isNotificaiton;
@property (assign) BOOL isNotificaitonFirstTime;
@property (assign) BOOL isFacebookTapped,isFromSideBarPayemnt;
@property (assign) NSUInteger userid;

+(SharedManager *)sharedInstance;
-(NSMutableArray *)filterArray:(NSMutableArray *)arrList WithClass:(Class)class;
@end
