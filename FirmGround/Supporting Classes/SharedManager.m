//
//  SharedManager.m
//  BNE
//
//  Created by Maurya on 03/02/15.
//  Copyright (c) 2015 i-verve infoWeb. All rights reserved.
//

#import "SharedManager.h"

static SharedManager *objSharedManager;

@implementation SharedManager
@synthesize isNetAvailable = _isNetAvailable;
@synthesize deviceToken;
@synthesize deviceType=_deviceType;
@synthesize instagramAccessToken =_instagramAccessToken;
@synthesize isFacebookTapped;
@synthesize arrSelectedPlateFrom = _arrSelectedPlateFrom;
+(SharedManager *)sharedInstance
{
    if(objSharedManager == nil)
    {
        objSharedManager = [[SharedManager alloc] init];
        
        objSharedManager.arrUserList =[[NSMutableArray alloc]init];
        objSharedManager.arrCardList =[[NSMutableArray alloc]init];
        objSharedManager.arrSelectedPlateFrom =[[NSMutableArray alloc]init];

        objSharedManager.TotalLikesOfUser = 0;
        
//        if(internetStatus == NotReachable)
//        {
//            NSLog(@"Internet Disconnected");
//            objSharedManager.isNetAvailable = NO;  // Internet not Connected
//        }
//        else if (internetStatus == ReachableViaWiFi)
//        {
//            NSLog(@"Connected via WIFI");
//            objSharedManager.isNetAvailable = YES; // Connected via WIFI
//        }
//        else if (internetStatus == ReachableViaWWAN)
//        {
//            NSLog(@"Connected via WWAN");
//            objSharedManager.isNetAvailable = YES; // Connected via WWAN
//        }
        if (objSharedManager.deviceToken.length <= 0) {
            objSharedManager.deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
        }
    }
    return objSharedManager;
}
-(NSMutableArray *)filterArray:(NSMutableArray *)arrList WithClass:(Class)class
{
    NSMutableArray *arrFilterArray = [[NSMutableArray alloc] initWithCapacity:arrList.count];
    
    for(NSMutableDictionary *dicUser in arrList)
    {
        id objUser = [[class alloc] init];
        
        for(NSString *key in dicUser.allKeys)
        {
            @try
            {
                if([[dicUser objectForKey:key] isEqual:[NSNull null]]) [objUser setValue:@"" forKey:key];
                else [objUser setValue:[dicUser objectForKey:key] forKey:key];
            }
            @catch(id anException)
            {
                NSLog(@"%@",anException);
            }
        }
        
        [arrFilterArray addObject:objUser];
    }
    return arrFilterArray;
}
@end
