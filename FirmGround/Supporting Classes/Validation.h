//
//  Validation.h
//  GigGuideLive
//
//  Created by Mac-1 on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Validation : NSObject<UITextFieldDelegate> {

}

+ (BOOL) validateEmail: (NSString *) candidate;	
+ (BOOL) textFieldLength: (NSString*) txtField;
+ (BOOL) validateContactNumber: (NSString*) ContactNo;
//+ (BOOL) validateURL :(NSString*)URLAddress;
+ (BOOL) validateUrl: (NSString *) URLAddress;
+(BOOL)validateString:(NSString *)str;
+(BOOL)validatePassword:(NSString *)str;
//+(BOOL) validateAge :(NSString *)Age
//;

@end
