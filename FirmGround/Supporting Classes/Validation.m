//
//  Validation.m
//  GigGuideLive
//
//  Created by Mac-1 on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Validation.h"


@implementation Validation

+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL) textFieldLength: (NSString*) txtField {
    
    if([txtField length] <= 0) {
        return NO;
    }
    
    return YES;
    
}
//+(BOOL) validateAge :(NSString *)Age
//{
////    NSString *AgeRegex=@"(100(0+)?|[1-9]?[0-9]([0-9]{1,2})";
////    NSPredicate *agetest = [NSPredicate predicateWithFormat:@"SELF MATCHS %@", AgeRegex];
////    return [agetest evaluateWithObject:Age];
//}

+(BOOL) validateContactNumber: (NSString*)ContactNo{
    NSString *phoneRegex = @"^([7-9]{1})([0-9]{9})$"; //"^9\\d{9}$"-(like 9876098998 only)
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:ContactNo];
}
//+ (BOOL) validateURL :(NSString*)URLAddress {
//	NSString *urlRegex = @" (http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
//	NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
//	return [urlTest evaluateWithObject:URLAddress];
//}
+ (BOOL) validateUrl: (NSString *) URLAddress {
    NSString *urlRegEx=@"(http|https)://((\\w <smb://w>)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w <smb://w>)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:URLAddress];
}
+(BOOL)validateString:(NSString *)str
{
    NSString *txtString=@"[A-Za-z]+";
    NSPredicate *name=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",txtString];
    return [name evaluateWithObject:str];
}

+(BOOL)validatePassword:(NSString *)str
{
    NSString *password=@"((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    NSPredicate *name=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",password];
    return [name evaluateWithObject:str];
}
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if (textField == txtUserName) {
//        NSString *str2=@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
//        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:str2] invertedSet];
//        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
//        if (![string isEqualToString:filtered]) {
//            [[NSString stringWithFormat:@"%@ is not allowed here...",string] ShowAsAlert];
//        }
//        return [string isEqualToString:filtered];
//    }
//    return YES;
//}
//-(NSString *)xhk:(NSRange)range replacementString:(NSString *)string{
//    NSString *str2=@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
//    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:str2] invertedSet];
//    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
//    if (![string isEqualToString:filtered]) {
//        [[NSString stringWithFormat:@"%@ is not allowed here...",string] ShowAsAlert];
//    }
//    return [string isEqualToString:filtered];
//}
@end
