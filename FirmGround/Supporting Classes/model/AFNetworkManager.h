//
//  AFNetworkManager.h
//  AFNetworkingAPIDemo
//
//  Created by Piyush Patel on 18/06/15.
//  Copyright (c) 2015 Piyush Patel All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "NSDictionary+SJSONString.h"
#import "AFNetworkActivityIndicatorManager.h"


@interface AFNetworkManager : NSObject
{
    AFHTTPSessionManager *_sharedClient;
    UIActivityIndicatorView *indicator;
}

+(void)getResponseServiceCallWithParameter:(NSDictionary*)param strURL:(NSString*)stringURL :(void (^)(id response))success
                                   failure:(void (^)(NSError *error))failure;

+(void)postOperationServiceCallWithParameter:(NSDictionary *)param strURL:(NSString *)stringURL :(void (^)(id))success
                                     failure:(void (^)(NSError *))failure;

+(void)startDownloadTaskWithNSURL:(NSURL *)URL :(void(^)(id))success DirectoryURL:(void(^)(NSURL*))directoryURL
                          failure:(void (^)(NSError *))failure;

+(void)startUploadTaskWithStringURL:(NSURL*)baseURL filePathURL:(NSURL *)URLfilePath :(void(^)(NSURLResponse *response))respon responceObject:(void(^)(id responseObject))responObject failure:(void(^)(NSError*))failure;

+(void)getwithAFHTTPRequestOperationwithUrlRequest:(NSURL*)ReqURL :(void (^)(id))success
                                           failure:(void (^)(NSError *))failure;

+ (NSURLSessionDataTask *)globalTimelineResponceWithParameters:(NSDictionary *)param stringURL:(NSString *)URLstring  :(void (^)(NSArray *arrResponce, NSError *error))block;

-(void)SharedNetworkReachability;
-(void)HTTPManagerReachabilityWithBaseURL:(NSURL*)baseURL;


//- (NSURLSessionDataTask *)GET:(NSString *)URLString
//                   parameters:(NSDictionary *)parameters
//                      success:(void (^)(NSURLSessionDataTask *, id))success
//                      failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
//
//- (NSURLSessionDataTask *)HEAD:(NSString *)URLString
//                    parameters:(NSDictionary *)parameters
//                       success:(void (^)(NSURLSessionDataTask *))success
//                       failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
//
//- (NSURLSessionDataTask *)POST:(NSString *)URLString
//                    parameters:(NSDictionary *)parameters
//                       success:(void (^)(NSURLSessionDataTask *, id))success
//                       failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
//
//- (NSURLSessionDataTask *)POST:(NSString *)URLString
//                    parameters:(NSDictionary *)parameters
//     constructingBodyWithBlock:(void (^)(id <AFMultipartFormData>))block
//                       success:(void (^)(NSURLSessionDataTask *, id))success
//                       failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
//
//- (NSURLSessionDataTask *)PUT:(NSString *)URLString
//                   parameters:(NSDictionary *)parameters
//                      success:(void (^)(NSURLSessionDataTask *, id))success
//                      failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
//
//- (NSURLSessionDataTask *)PATCH:(NSString *)URLString
//                     parameters:(NSDictionary *)parameters
//                        success:(void (^)(NSURLSessionDataTask *))success
//                        failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
//
//- (NSURLSessionDataTask *)DELETE:(NSString *)URLString
//                      parameters:(NSDictionary *)parameters
//                         success:(void (^)(NSURLSessionDataTask *, id))success
//                         failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;

@end
