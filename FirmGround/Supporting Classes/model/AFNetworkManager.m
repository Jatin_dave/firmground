//
//  AFNetworkManager.m
//  AFNetworkingAPIDemo
//
//  Created by Piyush Patel on 18/06/15.
//  Copyright (c) 2015 PiyushPatel. All rights reserved.
//

#import "AFNetworkManager.h"
#import "Constant.h"

@interface AFAppDotNetAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end

@implementation AFAppDotNetAPIClient

+ (instancetype)sharedClient {
    static AFAppDotNetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[AFAppDotNetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.app.net/"]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

@end

@implementation AFNetworkManager

#pragma mark
#pragma mark AFHTTPSessionManager

+ (NSURLSessionDataTask *)globalTimelineResponceWithParameters:(NSDictionary *)param stringURL:(NSString *)URLstring  :(void (^)(NSArray *arrResponce, NSError *error))block {
    return [[AFAppDotNetAPIClient sharedClient] GET:URLstring parameters:param success:^(NSURLSessionDataTask * __unused task, id JSON)
            {
                NSArray *arrGetResponse = [JSON valueForKeyPath:@"data"];
                NSMutableArray *mutArrResponce = [NSMutableArray arrayWithCapacity:[arrGetResponse count]];
                for (NSDictionary *attributes in arrGetResponse) {
                    [mutArrResponce addObject:attributes];
                }
                
                if (block) {
                    block([NSArray arrayWithArray:mutArrResponce], nil);
                }
            } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                if (block) {
                    block([NSArray array], error);
                }
            }];
}

#pragma mark
#pragma mark HTTP Request Operation Manager

+(void)getResponseServiceCallWithParameter:(NSDictionary*)param strURL:(NSString*)stringURL :(void (^)(id response))success
                                   failure:(void (^)(NSError *error))failure;
{
    
//    AFNetworkReachabilityManager *afReachability = [AFNetworkReachabilityManager managerForDomain:@"www.google.com"];
//    [afReachability setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//        if (status < AFNetworkReachabilityStatusReachableViaWWAN) {
//            NSLog(@"No internet connection");
//        }
//    }];
//    
//    [afReachability startMonitoring];
    
     // [[[self alloc] init] HTTPManagerReachabilityWithBaseURL:[NSURL URLWithString:stringURL]];
    
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager]setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        NSLog(@"%ld", (long)status);
        
        if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWWAN ||
            [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWiFi ) {
            
            NSLog(@"Connected To Internet");
            
            @try {
                NSLog(@"Request - %@", [param jsonStringWithPrettyPrint:YES]);
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                [manager GET:stringURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    success(responseObject);
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    failure(error);
                }];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception - %@", [exception debugDescription]);
            }
            @finally {
                
            }
        }
        else
        {
            NSLog(@"fail to Connect with Internet");
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                            message:@"Internet Connection Error"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dissmiss"
                                                  otherButtonTitles:nil,nil];
            [alert show];
            
        }
    }];
    
}

+(void)postOperationServiceCallWithParameter:(NSDictionary *)param strURL:(NSString *)stringURL :(void (^)(id))success failure:(void (^)(NSError *))failure
{
    
    
    //[[[self alloc] init] HTTPManagerReachabilityWithBaseURL:[NSURL URLWithString:stringURL]];

    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager]setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        NSLog(@"%ld", (long)status);
        
        if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWWAN ||
            [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWiFi ) {
            
            NSLog(@"Connected To Internet");
            @try {
                NSLog(@"Request - %@", [param jsonStringWithPrettyPrint:YES]);
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.requestSerializer = [AFJSONRequestSerializer serializer];
                [manager POST:stringURL
                   parameters:param
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          // NSLog(@"JSON: %@", responseObject);
                          success(responseObject);
                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          NSLog(@"Error: %@", error);
                          failure(error);
                      }];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception - %@", [exception debugDescription]);
            }
            @finally {
                
            }
        }
        else {
            NSLog(@"fail to Connect with Internet");
          UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                            message:@"Internet Connection Error"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dissmiss"
                                                  otherButtonTitles:nil,nil];
            [alert show];
            
        }
    }];
}

#pragma mark -
#pragma mark AFHTTPRequestOperation

+(void)getwithAFHTTPRequestOperationwithUrlRequest:(NSURL*)ReqURL :(void (^)(id))success failure:(void (^)(NSError *))failure
{
   // NSURL *URL = [NSURL URLWithString:@"http://example.com/resources/123.json"];
    NSURLRequest *request = [NSURLRequest requestWithURL:ReqURL];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
       // NSLog(@"JSON: %@", responseObject);
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
}

#pragma mark -
#pragma mark AFURLSessionManager

+(void)startDownloadTaskWithNSURL:(NSURL *)URL :(void(^)(id))success DirectoryURL:(void(^)(NSURL*))directoryURL failure:(void (^)(NSError *))failure
{
    @try {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
       // NSURL *URL = [NSURL URLWithString:@"http://www.hdwallpapersimages.com/wp-content/uploads/Greenary-Nature-Waterfall-HD-Images.jpg"];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            NSLog(@"File downloaded to: %@", filePath);
            success(response);
            failure(error);
            NSLog(@"responce ==> %@",response);
            directoryURL(filePath);
        }];
        [downloadTask resume];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception - %@", [exception debugDescription]);
    }
    @finally {
        
    }
}

+(void)startUploadTaskWithStringURL:(NSURL*)baseURL filePathURL:(NSURL *)URLfilePath :(void(^)(NSURLResponse *response))respon responceObject:(void(^)(id responseObject))responObject failure:(void(^)(NSError*))failure;
{
    @try {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        //NSURL *URL = [NSURL URLWithString:@"http://example.com/upload"];
        NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
        
         // NSURL *filePath = [NSURL fileURLWithPath:@"file://path/to/image.png"];
        NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithRequest:request fromFile:URLfilePath progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                failure(error);
            } else {
               // NSLog(@"Success: %@ %@", response, responseObject);
                respon(response);
                responObject(responseObject);
            }
        }];
        [uploadTask resume];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception - %@", [exception debugDescription]);
        
    }
    @finally {
        
    }
}

#pragma mark-

#pragma mark Network Reachability Manager

-(void)SharedNetworkReachability
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    }];
}

-(void)HTTPManagerReachabilityWithBaseURL:(NSURL*)baseURL;
{
   // NSURL *baseURL = [NSURL URLWithString:@"http://example.com/"];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    
    NSOperationQueue *operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [operationQueue setSuspended:NO];
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                [operationQueue setSuspended:YES];
                break;
        }
    }];
    
    [manager.reachabilityManager startMonitoring];
    
}
#pragma mark -

@end
