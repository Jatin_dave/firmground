//
//  CreateGroupCVCell.h
//  FirmGround
//
//  Created by Admin on 24/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactList.h"

@interface CreateGroupCVCell : UICollectionViewCell{
    
}
-(void)InitwithCustumCellwithIndexpath :(NSIndexPath *)indexpath withUser:(ContactList *)user;

@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;

@end
