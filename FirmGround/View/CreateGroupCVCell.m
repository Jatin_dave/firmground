//
//  CreateGroupCVCell.m
//  FirmGround
//
//  Created by Admin on 24/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CreateGroupCVCell.h"

@implementation CreateGroupCVCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width/2;
    _imgProfile.layer.masksToBounds = YES;
    _imgProfile.layer.borderWidth = 3;
}
-(void)InitwithCustumCellwithIndexpath :(NSIndexPath *)indexpath withUser:(ContactList *)user{
    
    _imgProfile.layer.borderColor =user.borderClor.CGColor;

}
@end
