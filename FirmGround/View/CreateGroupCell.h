//
//  CreateGroupCell.h
//  FirmGround
//
//  Created by Admin on 24/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateGroupCell : UITableViewCell
{
    
}

-(UIColor *)InitwithCustumCellwithIndexpath :(NSIndexPath *)indexpath;

@property (strong, nonatomic) IBOutlet UIImageView *imgprofile;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *btnSelected;

@end
