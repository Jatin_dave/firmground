//
//  CreateGroupCell.m
//  FirmGround
//
//  Created by Admin on 24/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "CreateGroupCell.h"
#import "Constant.h"
#import <CoreGraphics/CoreGraphics.h>
@implementation CreateGroupCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    [self performSelector:@selector(new) withObject:nil afterDelay:0.3];
    
}
-(void)new
{
    _imgprofile.layer.cornerRadius = _imgprofile.frame.size.width/2;
    _imgprofile.layer.masksToBounds = YES;
    _imgprofile.layer.borderWidth = 5;
    //    _imgprofile.layer.borderColor = [UIColor whiteColor].CGColor;
}

-(UIColor *)InitwithCustumCellwithIndexpath :(NSIndexPath *)indexpath
{
    _imgprofile.layer.cornerRadius = _imgprofile.frame.size.width/2;
    _imgprofile.layer.masksToBounds = YES;
    _imgprofile.layer.borderWidth = 5;

    int frequency = indexpath.row %10;
    switch (frequency) {
        case 0:
            //green
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(45, 169, 99, 1).CGColor;

            break;
        case 1:
            //orange
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(242, 129, 36, 1).CGColor;

            break;
        case 2:
            //blue
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(43, 155, 221, 1).CGColor;

            break;
        case 3:
            
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(38, 168, 98, 1).CGColor;

            break;
        case 4:
            
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(38, 160, 161, 1).CGColor;

            break;
        case 5:
            
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(242, 129, 36, 1).CGColor;

            break;
        case 6:
            
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(38, 168, 98, 1).CGColor;

            break;
        case 7:
            
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(38, 160, 161, 1).CGColor;

            break;
        case 8:
            
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(242, 129, 36, 1).CGColor;
            
            break;
        case 9:
            
            _imgprofile.layer.borderColor = GET_COLOR_WITH_RGB(45, 169, 99, 1).CGColor;
            
            break;
            
        default:
            break;
    }
    
    UIColor *color = [UIColor colorWithCGColor:_imgprofile.layer.borderColor];
    
    return color;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
