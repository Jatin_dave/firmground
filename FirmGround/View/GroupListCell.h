//
//  GroupListCell.h
//  FirmGround
//
//  Created by Admin on 12/08/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgprofile;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lbllastMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblLastuserName;
-(UIColor *)InitwithCustumCellwithIndexpath :(NSIndexPath *)indexpath;

@end
