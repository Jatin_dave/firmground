//
//  ChatMessagesCell.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>

@protocol pushView <NSObject>

-(void)pushView;

@end

@interface ChatMessagesCell : UICollectionViewCell<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    NSMutableArray *arrHeightForCell;
    NSMutableArray *arrChatText;
    CGFloat keybordHeight;
    
    FIRDatabaseReference *dbRefUsers;
    FIRDatabaseHandle dbRefHandle;
    FIRDatabaseHandle dbRefRemove;
    
    NSMutableArray *arrCurrentUserData;
    
    int n;
    
}
@property (weak, nonatomic) IBOutlet UIButton *bynSendMsg;
@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTemp;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)btnSendMessage:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSendMessgeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSendMessageBottom;
-(void)textKeybordClosedAnimated:(BOOL)animated;
- (IBAction)nexBtn:(id)sender;
@property (weak,nonatomic) id<pushView>delegate;
@end
