//
//  ChatMessagesCell.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ChatMessagesCell.h"
#import "RightChatCell.h"
#import "LeftChatCell.h"

@implementation ChatMessagesCell

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.backgroundColor=[UIColor clearColor];
    _chatTableView.backgroundColor=[UIColor clearColor];
    
    arrChatText=[[NSMutableArray alloc]init];
    arrHeightForCell=[[NSMutableArray alloc]init];
    arrCurrentUserData=[[NSMutableArray alloc]init];
    
    _textView.autocorrectionType=UITextAutocorrectionTypeNo;
    
    _textView.layer.borderWidth=0.5;
    _textView.layer.borderColor=[UIColor lightGrayColor].CGColor;

    [self manageUChangedEventsOfDataBasedata:nil];
    
    UITapGestureRecognizer *tapgasture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tableTapped)];
    [_chatTableView addGestureRecognizer:tapgasture];
}


#pragma mark Manage Firebase Data

-(void)manageUChangedEventsOfDataBasedata:(NSDictionary*)dataDict{
    
    if (dataDict==nil) {
        
        dbRefUsers=[[FIRDatabase database] referenceWithPath:@"Users"];
      
    }
    
    [[dbRefUsers childByAutoId]setValue:dataDict];
    
    dbRefHandle= [dbRefUsers observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSDictionary *childAdded=snapshot.value;
        NSString *message = [childAdded valueForKey:@"ChatMessage"];
        
        [arrCurrentUserData addObject:childAdded];
        NSLog(@"%@",childAdded);
        
        if (dataDict==nil) {
            [self reloadTableWihMessage:message];
        }
    }];
    
    dbRefHandle= [dbRefUsers observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSDictionary *childChanged=snapshot.value;
        NSLog(@"%@",childChanged);
        
    }];
}


#pragma mark Reload Table With Message

-(void)reloadTableWihMessage:(NSString *)strMsg{
    
    [arrChatText  addObject:strMsg];
    _lblTemp.text=strMsg;
    [self layoutIfNeeded];
    [arrHeightForCell addObject:[NSString stringWithFormat:@"%f",_lblTemp.frame.size.height]];
    [_chatTableView reloadData];
    
    _textView.clipsToBounds=YES;
    _bynSendMsg.clipsToBounds=YES;
    
//    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
//    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
}
#pragma mark Layout Subviews

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    _textView.layer.cornerRadius=_textView.frame.size.height/2;
    _bynSendMsg.layer.cornerRadius=_bynSendMsg.frame.size.height/2;
    
    NSLog(@"%f",self.frame.size.width);
    if (self.frame.size.width==320) {
        NSLog(@"iphone5");
        keybordHeight=216;
        
    }
    else if (self.frame.size.width==375){
        NSLog(@"iphone6,7");
       keybordHeight=216;
        
    }
    else if (self.frame.size.width==414){
        NSLog(@"iphone7s");
        
    }
    
    
}

//-(void)keyboardOnScreen:(NSNotification *)notification
//{
//    NSDictionary *info  = notification.userInfo;
//    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
//    
//    CGRect rawFrame      = [value CGRectValue];
//    CGRect keyboardFrame = [self convertRect:rawFrame fromView:nil];
//    
//    NSLog(@"keyboardFrame: %@", NSStringFromCGRect(keyboardFrame));
//}
#pragma mark Table Gasture

-(void)tableTapped{
    [self setdefaultView];
}

#pragma mark Table View DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrChatText.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *strUserName = [[arrCurrentUserData objectAtIndex:indexPath.row] valueForKey:@"UserName"];
    NSLog(@"row:%ld",(long)indexPath.row);
    if ([strUserName isEqualToString:@"Punit"]) {
        
        LeftChatCell *cell=[tableView dequeueReusableCellWithIdentifier:@"leftCell"];
        cell.lblChatText.text=[arrChatText objectAtIndex:indexPath.row];
       // cell.lblUserName.text=strUserName;
        return cell;
    }
    else{
        RightChatCell *cell=[tableView dequeueReusableCellWithIdentifier:@"rightCell"];
      //  cell.lblUserName.text=strUserName;
        cell.lblChatText.text=[arrChatText objectAtIndex:indexPath.row];
        return cell;
    }
}

#pragma mark Table View Delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self setdefaultView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *str=[arrHeightForCell objectAtIndex:indexPath.row];
    NSLog(@"%@",str);
    return str.integerValue+60;
    
}

#pragma mark TextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView{
    [self textKeyborOpen];
}
-(void)textViewDidChange:(UITextView *)textView{
    
    NSLog(@"textView:%f",textView.contentSize.height);
    // _viewSendMessgeHeight.constant=textView.contentSize.height;
    
}

#pragma mark Button Actions

- (IBAction)btnSendMessage:(id)sender {
    // [self textKeybordClosedAnimated:YES];
    
    NSCharacterSet *inverted = [[NSCharacterSet whitespaceAndNewlineCharacterSet] invertedSet];
    NSRange range = [_textView.text rangeOfCharacterFromSet:inverted];
    BOOL empty = (range.location == NSNotFound);
    
    if (!empty) {
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
        NSString *strDate=[dateFormatter stringFromDate:[NSDate date]];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm:ss a"];
        NSLog(@"Current Date: %@", [formatter stringFromDate:[NSDate date]]);
        NSString *strTime=[formatter stringFromDate:[NSDate date]];
        
        
        
        NSDictionary *aDict=@{@"UserName":@"Punit",@"ChatMessage":_textView.text,@"Date":strDate,@"Time":strTime};
        
        [self manageUChangedEventsOfDataBasedata:aDict];
        
        [self scrollTableToTheBottom:YES];
        
        //  [self reloadTableWihMessage:_textView.text];
        _textView.text=@"";
        
    }
    
}

#pragma mark Keybord open close manage


-(void)textKeyborOpen{
    
    if (arrChatText.count==0) {
        //No scroll
    }else{
        [self scrollTableToTheBottom:YES];
    }
    _viewSendMessageBottom.constant=keybordHeight;
    [UIView animateWithDuration:0.28 animations:^{
        [self layoutIfNeeded];
    }];
}
-(void)textKeybordClosedAnimated:(BOOL)animated{
    [self endEditing:YES];
    _viewSendMessageBottom.constant=0;
    if (animated) {
        [UIView animateWithDuration:0.28 animations:^{
            [self layoutIfNeeded];
        }];
    }
}



#pragma mark Table Scroll to bottom

- (void)scrollTableToTheBottom:(BOOL)animated
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (arrChatText.count!=0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:arrChatText.count-1 inSection:0];
            [_chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        }
        
    });
}

-(void)setdefaultView{
    [self textKeybordClosedAnimated:YES];
    [self endEditing:YES];
    _textView.text=@"";
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}
- (IBAction)nexBtn:(id)sender {
    [[self delegate]pushView];
}
@end
