//
//  LeftChatCell.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 8/2/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftChatCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgWraper;
@property (weak, nonatomic) IBOutlet UILabel *lblChatText;

@end
