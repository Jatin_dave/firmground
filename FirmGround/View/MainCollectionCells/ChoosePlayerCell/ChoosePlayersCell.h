//
//  ChoosePlayersCell.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePlayersCell : UICollectionViewCell<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblDateShow;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UIView *viewIn;
@property (weak, nonatomic) IBOutlet UIView *viewOut;
@property (weak, nonatomic) IBOutlet UITableView *tableIn;
@property (weak, nonatomic) IBOutlet UITableView *tableOut;
@property (weak, nonatomic) IBOutlet UIButton *btnIn;
@property (weak, nonatomic) IBOutlet UIButton *btnOut;

@end
