//
//  ChoosePlayersCell.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ChoosePlayersCell.h"

@implementation ChoosePlayersCell


-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    _containerView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _containerView.layer.borderWidth=0.5;
    
    self.backgroundColor=[UIColor clearColor];
    
    _viewIn.layer.cornerRadius=6;
    _viewIn.clipsToBounds=YES;
    
    _viewOut.layer.cornerRadius=6;
    _viewOut.clipsToBounds=YES;
    
    _btnIn.layer.cornerRadius=6;
    _btnIn.clipsToBounds=YES;
    
    _btnOut.layer.cornerRadius=6;
    _btnOut.clipsToBounds=YES;
    
    _imgProfile.layer.cornerRadius=_imgProfile.frame.size.width/2;
    _imgProfile.clipsToBounds=YES;
    
    _containerView.layer.cornerRadius=6;
    _containerView.clipsToBounds=YES;
    
    [self addShadow];

}

#pragma mark Layout

//-(void)layoutSubviews{
//    
//   }

#pragma mark Add Shadow

-(void)addShadow{
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_containerView.bounds];
    _containerView.layer.masksToBounds = YES;
    _containerView.layer.shadowColor = [UIColor blackColor].CGColor;
    _containerView.layer.shadowOffset = CGSizeMake(-5.0f, -5.0f);  /*Change value of X n Y as per your need of shadow to appear to like right bottom or left bottom or so on*/
    _containerView.layer.shadowOpacity = 0.2f;
    _containerView.layer.shadowPath = shadowPath.CGPath;
    
}

#pragma mark TableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView==_tableIn){
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"inCell"];
        cell.textLabel.text=@"Name";
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
        [cell.textLabel setTextColor:[UIColor grayColor]];
        return cell;
    }
    else if (tableView==_tableOut){
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"outCell"];
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
        cell.textLabel.text=@"Name";
        [cell.textLabel setTextColor:[UIColor grayColor]];
        return cell;
    }
    return nil;
}

#pragma mark Tableview Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

@end
