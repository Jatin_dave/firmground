//
//  ConfirmGameCell.h
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAYCalendarView.h"

@interface ConfirmGameCell : UICollectionViewCell{
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *btnSetRemainder;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelGame;
@property (strong, nonatomic) IBOutlet UITextField *txttime;
@property (strong, nonatomic) IBOutlet UITextField *txtLocation;
@property (weak, nonatomic) IBOutlet UITextField *txtCalender;

@end
