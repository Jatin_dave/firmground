//
//  ConfirmGameCell.m
//  FirmGround
//
//  Created by Dhaval Trivedi on 7/27/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ConfirmGameCell.h"

@implementation ConfirmGameCell

-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    _containerView.layer.borderColor=[UIColor grayColor].CGColor;
    _containerView.layer.borderWidth=1;
    
    self.backgroundColor=[UIColor clearColor];
    

}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    _containerView.layer.cornerRadius=6;
    _containerView.clipsToBounds=YES;
    
    _btnCancelGame.layer.cornerRadius=4;
    _btnCancelGame.clipsToBounds=YES;
    
    _btnSetRemainder.layer.cornerRadius=4;
    _btnSetRemainder.clipsToBounds=YES;
    
    [self addShadow];
    
}
-(void)addShadow{
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_containerView.bounds];
    _containerView.layer.masksToBounds = YES;
    _containerView.layer.shadowColor = [UIColor blackColor].CGColor;
    _containerView.layer.shadowOffset = CGSizeMake(-5.0f, -5.0f);  /*Change value of X n Y as per your need of shadow to appear to like right bottom or left bottom or so on*/
    _containerView.layer.shadowOpacity = 0.2f;
    _containerView.layer.shadowPath = shadowPath.CGPath;
    
}
@end
